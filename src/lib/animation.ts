const animation = {
	v: "5.7.4",
	fr: 25,
	ip: 0,
	op: 250,
	w: 1890,
	h: 1350,
	nm: "42Roma",
	ddd: 0,
	assets: [],
	fonts: {
		list: [
			{
				origin: 0,
				fPath: "",
				fClass: "",
				fFamily: "Arial",
				fWeight: "",
				fStyle: "Bold",
				fName: "Arial-BoldMT",
				ascent: 71.5988159179688
			},
			{
				origin: 0,
				fPath: "",
				fClass: "",
				fFamily: "Arial",
				fWeight: "",
				fStyle: "Regular",
				fName: "ArialMT",
				ascent: 71.5988159179688
			}
		]
	},
	layers: [
		{
			ddd: 0,
			ind: 1,
			ty: 5,
			nm: "CITY",
			sr: 1,
			ks: {
				o: {
					a: 0,
					k: 100,
					ix: 11
				},
				r: {
					a: 0,
					k: 0,
					ix: 10
				},
				p: {
					s: true,
					x: {
						a: 0,
						k: 38.555,
						ix: 3
					},
					y: {
						a: 1,
						k: [
							{
								i: {
									x: [0.01],
									y: [1]
								},
								o: {
									x: [0.66],
									y: [0]
								},
								t: -2,
								s: [1353.698]
							},
							{
								t: 35,
								s: [1253.698]
							}
						],
						ix: 4
					}
				},
				a: {
					a: 0,
					k: [0, 0, 0],
					ix: 1,
					l: 2
				},
				s: {
					a: 0,
					k: [131.836, 131.836, 100],
					ix: 6,
					x: "var $bm_rt;\n$bm_rt = $bm_mul(Math.min($bm_div(640, sourceRectAtTime().width), 1), value);",
					l: 2
				}
			},
			ao: 0,
			t: {
				d: {
					k: [
						{
							s: {
								s: 180,
								f: "ArialMT",
								t: "Roma",
								j: 0,
								tr: 0,
								lh: 216,
								ls: 0,
								fc: [1, 1, 1]
							},
							t: 0
						}
					]
				},
				p: {},
				m: {
					g: 1,
					a: {
						a: 0,
						k: [0, 0],
						ix: 2
					}
				},
				a: []
			},
			ip: 12,
			op: 250,
			st: -2,
			bm: 0
		},
		{
			ddd: 0,
			ind: 2,
			ty: 5,
			nm: "Air quality index label",
			sr: 1,
			ks: {
				o: {
					a: 0,
					k: 100,
					ix: 11
				},
				r: {
					a: 0,
					k: 0,
					ix: 10
				},
				p: {
					s: true,
					x: {
						a: 0,
						k: 140.581,
						ix: 3
					},
					y: {
						a: 1,
						k: [
							{
								i: {
									x: [0.01],
									y: [1]
								},
								o: {
									x: [0.66],
									y: [0]
								},
								t: 0,
								s: [960.537]
							},
							{
								t: 37,
								s: [1000.537]
							}
						],
						ix: 4
					}
				},
				a: {
					a: 0,
					k: [0, 0, 0],
					ix: 1,
					l: 2
				},
				s: {
					a: 0,
					k: [131.836, 131.836, 100],
					ix: 6,
					l: 2
				}
			},
			ao: 0,
			t: {
				d: {
					k: [
						{
							s: {
								sz: [145.734252929688, 24.0690002441406],
								ps: [-72.8671264648438, -12.0345001220703],
								s: 20,
								f: "ArialMT",
								t: "Air quality index",
								j: 0,
								tr: 0,
								lh: 24,
								ls: 0,
								fc: [1, 1, 1]
							},
							t: 0
						}
					]
				},
				p: {},
				m: {
					g: 1,
					a: {
						a: 0,
						k: [0, 0],
						ix: 2
					}
				},
				a: []
			},
			ip: 14,
			op: 250,
			st: 0,
			bm: 0
		},
		{
			ddd: 0,
			ind: 3,
			ty: 4,
			nm: "MASK 7",
			parent: 9,
			td: 1,
			sr: 1,
			ks: {
				o: {
					a: 0,
					k: 100,
					ix: 11
				},
				r: {
					a: 0,
					k: 0,
					ix: 10
				},
				p: {
					s: true,
					x: {
						a: 0,
						k: 0,
						ix: 3
					},
					y: {
						a: 0,
						k: 0,
						ix: 4
					}
				},
				a: {
					a: 0,
					k: [0, 0, 0],
					ix: 1,
					l: 2
				},
				s: {
					a: 0,
					k: [100, 100, 100],
					ix: 6,
					l: 2
				}
			},
			ao: 0,
			shapes: [
				{
					ty: "gr",
					it: [
						{
							ty: "rc",
							d: 1,
							s: {
								a: 0,
								k: [721.176, 1024],
								ix: 2
							},
							p: {
								a: 0,
								k: [0, 0],
								ix: 3
							},
							r: {
								a: 0,
								k: 0,
								ix: 4
							},
							nm: "Rectangle Path 1",
							mn: "ADBE Vector Shape - Rect",
							hd: false
						},
						{
							ty: "fl",
							c: {
								a: 0,
								k: [0.339930534363, 0.741666674614, 0.669354140759, 1],
								ix: 4
							},
							o: {
								a: 0,
								k: 100,
								ix: 5
							},
							r: 1,
							bm: 0,
							nm: "Fill 1",
							mn: "ADBE Vector Graphic - Fill",
							hd: false
						},
						{
							ty: "tr",
							p: {
								a: 0,
								k: [0, 0],
								ix: 2
							},
							a: {
								a: 0,
								k: [0, 0],
								ix: 1
							},
							s: {
								a: 0,
								k: [100, 100],
								ix: 3
							},
							r: {
								a: 0,
								k: 0,
								ix: 6
							},
							o: {
								a: 0,
								k: 100,
								ix: 7
							},
							sk: {
								a: 0,
								k: 0,
								ix: 4
							},
							sa: {
								a: 0,
								k: 0,
								ix: 5
							},
							nm: "Transform"
						}
					],
					nm: "Rectangle 79",
					np: 2,
					cix: 2,
					bm: 0,
					ix: 1,
					mn: "ADBE Vector Group",
					hd: false
				}
			],
			ip: 0,
			op: 250,
			st: 0,
			bm: 0
		},
		{
			ddd: 0,
			ind: 4,
			ty: 5,
			nm: "DATE",
			tt: 1,
			sr: 1,
			ks: {
				o: {
					a: 0,
					k: 100,
					ix: 11
				},
				r: {
					a: 0,
					k: 0,
					ix: 10
				},
				p: {
					s: true,
					x: {
						a: 0,
						k: 52.563,
						ix: 3
					},
					y: {
						a: 0,
						k: 196.843,
						ix: 4
					}
				},
				a: {
					a: 0,
					k: [0, 0, 0],
					ix: 1,
					l: 2
				},
				s: {
					a: 0,
					k: [131.836, 131.836, 100],
					ix: 6,
					l: 2
				}
			},
			ao: 0,
			t: {
				d: {
					k: [
						{
							s: {
								s: 50,
								f: "ArialMT",
								t: "12|04|2022",
								j: 0,
								tr: 0,
								lh: 60,
								ls: 0,
								fc: [1, 1, 1]
							},
							t: 0
						}
					]
				},
				p: {},
				m: {
					g: 1,
					a: {
						a: 0,
						k: [0, 0],
						ix: 2
					}
				},
				a: []
			},
			ip: 0,
			op: 250,
			st: 0,
			bm: 0
		},
		{
			ddd: 0,
			ind: 5,
			ty: 4,
			nm: "MASK",
			parent: 9,
			td: 1,
			sr: 1,
			ks: {
				o: {
					a: 0,
					k: 100,
					ix: 11
				},
				r: {
					a: 0,
					k: 0,
					ix: 10
				},
				p: {
					s: true,
					x: {
						a: 0,
						k: 0,
						ix: 3
					},
					y: {
						a: 0,
						k: 0,
						ix: 4
					}
				},
				a: {
					a: 0,
					k: [0, 0, 0],
					ix: 1,
					l: 2
				},
				s: {
					a: 0,
					k: [100, 100, 100],
					ix: 6,
					l: 2
				}
			},
			ao: 0,
			shapes: [
				{
					ty: "gr",
					it: [
						{
							ty: "rc",
							d: 1,
							s: {
								a: 0,
								k: [721.176, 1024],
								ix: 2
							},
							p: {
								a: 0,
								k: [0, 0],
								ix: 3
							},
							r: {
								a: 0,
								k: 0,
								ix: 4
							},
							nm: "Rectangle Path 1",
							mn: "ADBE Vector Shape - Rect",
							hd: false
						},
						{
							ty: "fl",
							c: {
								a: 0,
								k: [0.339930534363, 0.741666674614, 0.669354140759, 1],
								ix: 4
							},
							o: {
								a: 0,
								k: 100,
								ix: 5
							},
							r: 1,
							bm: 0,
							nm: "Fill 1",
							mn: "ADBE Vector Graphic - Fill",
							hd: false
						},
						{
							ty: "tr",
							p: {
								a: 0,
								k: [0, 0],
								ix: 2
							},
							a: {
								a: 0,
								k: [0, 0],
								ix: 1
							},
							s: {
								a: 0,
								k: [100, 100],
								ix: 3
							},
							r: {
								a: 0,
								k: 0,
								ix: 6
							},
							o: {
								a: 0,
								k: 100,
								ix: 7
							},
							sk: {
								a: 0,
								k: 0,
								ix: 4
							},
							sa: {
								a: 0,
								k: 0,
								ix: 5
							},
							nm: "Transform"
						}
					],
					nm: "Rectangle 79",
					np: 2,
					cix: 2,
					bm: 0,
					ix: 1,
					mn: "ADBE Vector Group",
					hd: false
				}
			],
			ip: 0,
			op: 250,
			st: 0,
			bm: 0
		},
		{
			ddd: 0,
			ind: 6,
			ty: 5,
			nm: "Today's Air Quality",
			tt: 1,
			sr: 1,
			ks: {
				o: {
					a: 0,
					k: 100,
					ix: 11
				},
				r: {
					a: 0,
					k: 0,
					ix: 10
				},
				p: {
					s: true,
					x: {
						a: 0,
						k: 52.563,
						ix: 3
					},
					y: {
						a: 0,
						k: 108.843,
						ix: 4
					}
				},
				a: {
					a: 0,
					k: [0, 0, 0],
					ix: 1,
					l: 2
				},
				s: {
					a: 0,
					k: [131.836, 131.836, 100],
					ix: 6,
					l: 2
				}
			},
			ao: 0,
			t: {
				d: {
					k: [
						{
							s: {
								s: 45,
								f: "Arial-BoldMT",
								t: "Today’s Air Quality",
								j: 0,
								tr: 0,
								lh: 54,
								ls: 0,
								fc: [1, 1, 1]
							},
							t: 0
						}
					]
				},
				p: {},
				m: {
					g: 1,
					a: {
						a: 0,
						k: [0, 0],
						ix: 2
					}
				},
				a: []
			},
			ip: 0,
			op: 250,
			st: 0,
			bm: 0
		},
		{
			ddd: 0,
			ind: 7,
			ty: 5,
			nm: "Description",
			sr: 1,
			ks: {
				o: {
					a: 0,
					k: 100,
					ix: 11
				},
				r: {
					a: 0,
					k: 0,
					ix: 10
				},
				p: {
					s: true,
					x: {
						a: 0,
						k: 44.563,
						ix: 3
					},
					y: {
						a: 1,
						k: [
							{
								i: {
									x: [0.01],
									y: [1]
								},
								o: {
									x: [0.66],
									y: [0]
								},
								t: 41,
								s: [888.843]
							},
							{
								t: 71,
								s: [948.843]
							}
						],
						ix: 4
					}
				},
				a: {
					a: 0,
					k: [0, 0, 0],
					ix: 1,
					l: 2
				},
				s: {
					a: 0,
					k: [131.836, 131.836, 100],
					ix: 6,
					l: 2
				}
			},
			ao: 0,
			t: {
				d: {
					x: "var $bm_rt;\nvar v = thisComp.layer('Air Quality Index').effect('Index')('Slider').value;\n$bm_rt = v > 300 ? 'Hazardous' : v > 200 ? 'Very Unhealthy' : v > 100 ? 'Unhealthy' : v > 50 ? 'Moderate ' : 'Good';",
					k: [
						{
							s: {
								s: 45,
								f: "ArialMT",
								t: "Moderate",
								j: 0,
								tr: 0,
								lh: 54,
								ls: 0,
								fc: [1, 1, 1]
							},
							t: 0
						}
					]
				},
				p: {},
				m: {
					g: 1,
					a: {
						a: 0,
						k: [0, 0],
						ix: 2
					}
				},
				a: []
			},
			ip: 52,
			op: 250,
			st: 0,
			bm: 0
		},
		{
			ddd: 0,
			ind: 8,
			ty: 5,
			nm: "Index",
			sr: 1,
			ks: {
				o: {
					a: 0,
					k: 100,
					ix: 11
				},
				r: {
					a: 0,
					k: 0,
					ix: 10
				},
				p: {
					s: true,
					x: {
						a: 0,
						k: 44.563,
						ix: 3
					},
					y: {
						a: 1,
						k: [
							{
								i: {
									x: [0.01],
									y: [1]
								},
								o: {
									x: [0.66],
									y: [0]
								},
								t: 0,
								s: [984.843]
							},
							{
								i: {
									x: [0.01],
									y: [1]
								},
								o: {
									x: [0.66],
									y: [0]
								},
								t: 37,
								s: [944.843]
							},
							{
								i: {
									x: [0.01],
									y: [1]
								},
								o: {
									x: [0.66],
									y: [0]
								},
								t: 39,
								s: [944.843]
							},
							{
								t: 69,
								s: [884.843]
							}
						],
						ix: 4
					}
				},
				a: {
					a: 0,
					k: [-201.198, 5.575, 0],
					ix: 1,
					l: 2
				},
				s: {
					a: 0,
					k: [131.836, 131.836, 100],
					ix: 6,
					l: 2
				}
			},
			ao: 0,
			ef: [
				{
					ty: 5,
					nm: "Counter",
					np: 3,
					mn: "ADBE Slider Control",
					ix: 1,
					en: 1,
					ef: [
						{
							ty: 0,
							nm: "Slider",
							mn: "ADBE Slider Control-0001",
							ix: 1,
							v: {
								a: 1,
								k: [
									{
										i: {
											x: [0.01],
											y: [1]
										},
										o: {
											x: [0.4],
											y: [0]
										},
										t: 0,
										s: [0]
									},
									{
										t: 41,
										s: [1]
									}
								],
								ix: 1
							}
						}
					]
				}
			],
			t: {
				d: {
					x: "var $bm_rt;\n$bm_rt = Math.round($bm_mul(thisComp.layer('Air Quality Index').effect('Index')('Slider'), effect('Counter')('Slider')));",
					k: [
						{
							s: {
								sz: [410.467956542969, 83.2061920166016],
								ps: [-205.233978271484, -63.6030883789062],
								s: 95,
								f: "Arial-BoldMT",
								t: "62",
								j: 0,
								tr: 0,
								lh: 114,
								ls: 0,
								fc: [1, 1, 1]
							},
							t: 0
						}
					]
				},
				p: {},
				m: {
					g: 1,
					a: {
						a: 0,
						k: [0, 0],
						ix: 2
					}
				},
				a: []
			},
			ip: 14,
			op: 250,
			st: -2,
			bm: 0
		},
		{
			ddd: 0,
			ind: 9,
			ty: 4,
			nm: "Air Quality Index",
			sr: 1,
			ks: {
				o: {
					a: 0,
					k: 100,
					ix: 11
				},
				r: {
					a: 0,
					k: 0,
					ix: 10
				},
				p: {
					s: true,
					x: {
						a: 0,
						k: 471.166,
						ix: 3
					},
					y: {
						a: 1,
						k: [
							{
								i: {
									x: [0.01],
									y: [1]
								},
								o: {
									x: [0.66],
									y: [0]
								},
								t: 0,
								s: [2035]
							},
							{
								t: 30,
								s: [675]
							}
						],
						ix: 4
					}
				},
				a: {
					a: 0,
					k: [0, 0, 0],
					ix: 1,
					l: 2
				},
				s: {
					a: 0,
					k: [131.836, 131.836, 100],
					ix: 6,
					l: 2
				}
			},
			ao: 0,
			ef: [
				{
					ty: 5,
					nm: "Index",
					np: 3,
					mn: "ADBE Slider Control",
					ix: 1,
					en: 1,
					ef: [
						{
							ty: 0,
							nm: "Slider",
							mn: "ADBE Slider Control-0001",
							ix: 1,
							v: {
								a: 0,
								k: 62,
								ix: 1
							}
						}
					]
				}
			],
			shapes: [
				{
					ty: "gr",
					it: [
						{
							ty: "rc",
							d: 1,
							s: {
								a: 0,
								k: [721.176, 1024],
								ix: 2
							},
							p: {
								a: 0,
								k: [0, 0],
								ix: 3
							},
							r: {
								a: 0,
								k: 0,
								ix: 4
							},
							nm: "Rectangle Path 1",
							mn: "ADBE Vector Shape - Rect",
							hd: false
						},
						{
							ty: "fl",
							c: {
								a: 0,
								k: [0.339930534363, 0.741666674614, 0.669354140759, 1],
								ix: 4,
								x: "var $bm_rt;\nvar v = thisLayer.effect('Index')('Slider').value;\nif (v <= 50) {\n    $bm_rt = thisComp.layer('Level 1').content('Fill 1').color;\n} else if (v <= 100) {\n    $bm_rt = thisComp.layer('Level 2').content('Fill 1').color;\n} else if (v <= 200) {\n    $bm_rt = thisComp.layer('Level 3').content('Fill 1').color;\n} else if (v <= 300) {\n    $bm_rt = thisComp.layer('Level 4').content('Fill 1').color;\n} else {\n    $bm_rt = thisComp.layer('Level 5').content('Fill 1').color;\n}"
							},
							o: {
								a: 0,
								k: 100,
								ix: 5
							},
							r: 1,
							bm: 0,
							nm: "Fill 1",
							mn: "ADBE Vector Graphic - Fill",
							hd: false
						},
						{
							ty: "tr",
							p: {
								a: 0,
								k: [0, 0],
								ix: 2
							},
							a: {
								a: 0,
								k: [0, 0],
								ix: 1
							},
							s: {
								a: 0,
								k: [100, 100],
								ix: 3
							},
							r: {
								a: 0,
								k: 0,
								ix: 6
							},
							o: {
								a: 0,
								k: 100,
								ix: 7
							},
							sk: {
								a: 0,
								k: 0,
								ix: 4
							},
							sa: {
								a: 0,
								k: 0,
								ix: 5
							},
							nm: "Transform"
						}
					],
					nm: "Rectangle 79",
					np: 2,
					cix: 2,
					bm: 0,
					ix: 1,
					mn: "ADBE Vector Group",
					hd: false
				}
			],
			ip: 0,
			op: 250,
			st: 0,
			bm: 0
		},
		{
			ddd: 0,
			ind: 10,
			ty: 4,
			nm: "Level 1",
			parent: 9,
			sr: 1,
			ks: {
				o: {
					a: 0,
					k: 100,
					ix: 11
				},
				r: {
					a: 0,
					k: 0,
					ix: 10
				},
				p: {
					a: 0,
					k: [1.012, 303.407, 0],
					ix: 2,
					l: 2
				},
				a: {
					a: 0,
					k: [0, 0, 0],
					ix: 1,
					l: 2
				},
				s: {
					a: 0,
					k: [37.926, 37.926, 100],
					ix: 6,
					l: 2
				}
			},
			ao: 0,
			shapes: [
				{
					d: 1,
					ty: "el",
					s: {
						a: 0,
						k: [790, 790],
						ix: 2
					},
					p: {
						a: 0,
						k: [0, 0],
						ix: 3
					},
					nm: "Ellipse Path 1",
					mn: "ADBE Vector Shape - Ellipse",
					hd: false
				},
				{
					ty: "fl",
					c: {
						a: 0,
						k: [0.341176470588, 0.741176470588, 0.670588235294, 1],
						ix: 4
					},
					o: {
						a: 0,
						k: 100,
						ix: 5
					},
					r: 1,
					bm: 0,
					nm: "Fill 1",
					mn: "ADBE Vector Graphic - Fill",
					hd: false
				}
			],
			ip: 0,
			op: 269,
			st: 0,
			bm: 0
		},
		{
			ddd: 0,
			ind: 11,
			ty: 4,
			nm: "Level 2",
			parent: 9,
			sr: 1,
			ks: {
				o: {
					a: 0,
					k: 100,
					ix: 11
				},
				r: {
					a: 0,
					k: 0,
					ix: 10
				},
				p: {
					a: 0,
					k: [1.012, 151.704, 0],
					ix: 2,
					l: 2
				},
				a: {
					a: 0,
					k: [0, 0, 0],
					ix: 1,
					l: 2
				},
				s: {
					a: 0,
					k: [37.926, 37.926, 100],
					ix: 6,
					l: 2
				}
			},
			ao: 0,
			shapes: [
				{
					d: 1,
					ty: "el",
					s: {
						a: 0,
						k: [790, 790],
						ix: 2
					},
					p: {
						a: 0,
						k: [0, 0],
						ix: 3
					},
					nm: "Ellipse Path 1",
					mn: "ADBE Vector Shape - Ellipse",
					hd: false
				},
				{
					ty: "fl",
					c: {
						a: 0,
						k: [0.729411764706, 0.850980392157, 0.478431372549, 1],
						ix: 4
					},
					o: {
						a: 0,
						k: 100,
						ix: 5
					},
					r: 1,
					bm: 0,
					nm: "Fill 1",
					mn: "ADBE Vector Graphic - Fill",
					hd: false
				}
			],
			ip: 0,
			op: 273,
			st: 0,
			bm: 0
		},
		{
			ddd: 0,
			ind: 12,
			ty: 4,
			nm: "Level 3",
			parent: 9,
			sr: 1,
			ks: {
				o: {
					a: 0,
					k: 100,
					ix: 11
				},
				r: {
					a: 0,
					k: 0,
					ix: 10
				},
				p: {
					a: 0,
					k: [1.012, 0, 0],
					ix: 2,
					l: 2
				},
				a: {
					a: 0,
					k: [0, 0, 0],
					ix: 1,
					l: 2
				},
				s: {
					a: 0,
					k: [37.926, 37.926, 100],
					ix: 6,
					l: 2
				}
			},
			ao: 0,
			shapes: [
				{
					d: 1,
					ty: "el",
					s: {
						a: 0,
						k: [790, 790],
						ix: 2
					},
					p: {
						a: 0,
						k: [0, 0],
						ix: 3
					},
					nm: "Ellipse Path 1",
					mn: "ADBE Vector Shape - Ellipse",
					hd: false
				},
				{
					ty: "fl",
					c: {
						a: 0,
						k: [0.952941176471, 0.709803921569, 0.247058823529, 1],
						ix: 4
					},
					o: {
						a: 0,
						k: 100,
						ix: 5
					},
					r: 1,
					bm: 0,
					nm: "Fill 1",
					mn: "ADBE Vector Graphic - Fill",
					hd: false
				}
			],
			ip: 0,
			op: 277,
			st: 0,
			bm: 0
		},
		{
			ddd: 0,
			ind: 13,
			ty: 4,
			nm: "Level 4",
			parent: 9,
			sr: 1,
			ks: {
				o: {
					a: 0,
					k: 100,
					ix: 11
				},
				r: {
					a: 0,
					k: 0,
					ix: 10
				},
				p: {
					a: 0,
					k: [1.012, -151.704, 0],
					ix: 2,
					l: 2
				},
				a: {
					a: 0,
					k: [0, 0, 0],
					ix: 1,
					l: 2
				},
				s: {
					a: 0,
					k: [37.926, 37.926, 100],
					ix: 6,
					l: 2
				}
			},
			ao: 0,
			shapes: [
				{
					d: 1,
					ty: "el",
					s: {
						a: 0,
						k: [790, 790],
						ix: 2
					},
					p: {
						a: 0,
						k: [0, 0],
						ix: 3
					},
					nm: "Ellipse Path 1",
					mn: "ADBE Vector Shape - Ellipse",
					hd: false
				},
				{
					ty: "fl",
					c: {
						a: 0,
						k: [0.972549019608, 0.352941176471, 0.156862745098, 1],
						ix: 4
					},
					o: {
						a: 0,
						k: 100,
						ix: 5
					},
					r: 1,
					bm: 0,
					nm: "Fill 1",
					mn: "ADBE Vector Graphic - Fill",
					hd: false
				}
			],
			ip: 0,
			op: 281,
			st: 0,
			bm: 0
		},
		{
			ddd: 0,
			ind: 14,
			ty: 4,
			nm: "Level 5",
			parent: 9,
			sr: 1,
			ks: {
				o: {
					a: 0,
					k: 100,
					ix: 11
				},
				r: {
					a: 0,
					k: 0,
					ix: 10
				},
				p: {
					a: 0,
					k: [1.012, -303.407, 0],
					ix: 2,
					l: 2
				},
				a: {
					a: 0,
					k: [0, 0, 0],
					ix: 1,
					l: 2
				},
				s: {
					a: 0,
					k: [37.926, 37.926, 100],
					ix: 6,
					l: 2
				}
			},
			ao: 0,
			shapes: [
				{
					d: 1,
					ty: "el",
					s: {
						a: 0,
						k: [790, 790],
						ix: 2
					},
					p: {
						a: 0,
						k: [0, 0],
						ix: 3
					},
					nm: "Ellipse Path 1",
					mn: "ADBE Vector Shape - Ellipse",
					hd: false
				},
				{
					ty: "fl",
					c: {
						a: 0,
						k: [0.58431372549, 0.172549019608, 0.780392156863, 1],
						ix: 4
					},
					o: {
						a: 0,
						k: 100,
						ix: 5
					},
					r: 1,
					bm: 0,
					nm: "Fill 1",
					mn: "ADBE Vector Graphic - Fill",
					hd: false
				}
			],
			ip: 0,
			op: 285,
			st: 0,
			bm: 0
		},
		{
			ddd: 0,
			ind: 15,
			ty: 5,
			nm: "VALUE_3",
			sr: 1,
			ks: {
				o: {
					a: 0,
					k: 100,
					ix: 11
				},
				r: {
					a: 0,
					k: 0,
					ix: 10
				},
				p: {
					a: 1,
					k: [
						{
							i: {
								x: 0.01,
								y: 1
							},
							o: {
								x: 0.66,
								y: 0
							},
							t: 148,
							s: [1850.735, 1233.376, 0],
							to: [0, -5, 0],
							ti: [0, 5, 0]
						},
						{
							t: 185,
							s: [1850.735, 1203.376, 0]
						}
					],
					ix: 2,
					l: 2
				},
				a: {
					a: 0,
					k: [0, 0, 0],
					ix: 1,
					l: 2
				},
				s: {
					a: 0,
					k: [131.836, 131.836, 100],
					ix: 6,
					l: 2
				}
			},
			ao: 0,
			ef: [
				{
					ty: 5,
					nm: "Value",
					np: 3,
					mn: "ADBE Slider Control",
					ix: 1,
					en: 1,
					ef: [
						{
							ty: 0,
							nm: "Slider",
							mn: "ADBE Slider Control-0001",
							ix: 1,
							v: {
								a: 0,
								k: 148,
								ix: 1,
								x: "var $bm_rt;\n$bm_rt = thisComp.layer('Righette_Dato_3').effect('Today')('Slider');"
							}
						}
					]
				},
				{
					ty: 5,
					nm: "Counter",
					np: 3,
					mn: "ADBE Slider Control",
					ix: 2,
					en: 1,
					ef: [
						{
							ty: 0,
							nm: "Slider",
							mn: "ADBE Slider Control-0001",
							ix: 1,
							v: {
								a: 1,
								k: [
									{
										i: {
											x: [0.01],
											y: [1]
										},
										o: {
											x: [0.167],
											y: [0.167]
										},
										t: 164,
										s: [70]
									},
									{
										t: 188,
										s: [100]
									}
								],
								ix: 1
							}
						}
					]
				},
				{
					ty: 5,
					nm: "Color",
					np: 3,
					mn: "ADBE Color Control",
					ix: 3,
					en: 1,
					ef: [
						{
							ty: 2,
							nm: "Color",
							mn: "ADBE Color Control-0001",
							ix: 1,
							v: {
								a: 0,
								k: [1, 0, 0, 1],
								ix: 1,
								x: "var $bm_rt;\nvar v = thisLayer.effect('Value')('Slider').value;\nif (v < 50) {\n    $bm_rt = thisComp.layer('Level 1').content('Fill 1').color;\n} else if (v < 70) {\n    $bm_rt = thisComp.layer('Level 2').content('Fill 1').color;\n} else if (v < 100) {\n    $bm_rt = thisComp.layer('Level 3').content('Fill 1').color;\n} else if (v < 150) {\n    $bm_rt = thisComp.layer('Level 4').content('Fill 1').color;\n} else {\n    $bm_rt = thisComp.layer('Level 5').content('Fill 1').color;\n}"
							}
						}
					]
				}
			],
			t: {
				d: {
					x: "var $bm_rt;\n$bm_rt = Math.round($bm_div($bm_mul(effect('Value')('Slider'), effect('Counter')('Slider')), 100));",
					k: [
						{
							s: {
								s: 55,
								f: "Arial-BoldMT",
								t: "8.2",
								j: 1,
								tr: 0,
								lh: 66,
								ls: 0,
								fc: [0.161, 0.8, 0.694]
							},
							t: 0
						}
					]
				},
				p: {},
				m: {
					g: 1,
					a: {
						a: 0,
						k: [0, 0],
						ix: 2
					}
				},
				a: [
					{
						nm: "Color",
						s: {
							t: 0,
							xe: {
								a: 0,
								k: 0,
								ix: 7
							},
							ne: {
								a: 0,
								k: 0,
								ix: 8
							},
							a: {
								a: 0,
								k: 100,
								ix: 4
							},
							b: 1,
							rn: 0,
							sh: 1,
							r: 1
						},
						a: {
							fc: {
								a: 0,
								k: [0.160784319043, 0.800000011921, 0.694117665291, 1],
								ix: 12,
								x: "var $bm_rt;\n$bm_rt = effect('Color')('Color');"
							}
						}
					}
				]
			},
			ip: 163,
			op: 275,
			st: 152,
			bm: 0
		},
		{
			ddd: 0,
			ind: 16,
			ty: 5,
			nm: "VALUE_2",
			sr: 1,
			ks: {
				o: {
					a: 0,
					k: 100,
					ix: 11
				},
				r: {
					a: 0,
					k: 0,
					ix: 10
				},
				p: {
					a: 1,
					k: [
						{
							i: {
								x: 0.01,
								y: 1
							},
							o: {
								x: 0.66,
								y: 0
							},
							t: 75,
							s: [1850.735, 1233.376, 0],
							to: [0, -5, 0],
							ti: [0, 5, 0]
						},
						{
							i: {
								x: 0.01,
								y: 0.01
							},
							o: {
								x: 0.66,
								y: 0.66
							},
							t: 112,
							s: [1850.735, 1203.376, 0],
							to: [0, 0, 0],
							ti: [0, 0, 0]
						},
						{
							i: {
								x: 0.01,
								y: 1
							},
							o: {
								x: 0.66,
								y: 0
							},
							t: 148,
							s: [1850.735, 1203.376, 0],
							to: [0, -13.333, 0],
							ti: [0, 13.333, 0]
						},
						{
							t: 185,
							s: [1850.735, 1123.376, 0]
						}
					],
					ix: 2,
					l: 2
				},
				a: {
					a: 0,
					k: [0, 0, 0],
					ix: 1,
					l: 2
				},
				s: {
					a: 0,
					k: [131.836, 131.836, 100],
					ix: 6,
					l: 2
				}
			},
			ao: 0,
			ef: [
				{
					ty: 5,
					nm: "Value",
					np: 3,
					mn: "ADBE Slider Control",
					ix: 1,
					en: 1,
					ef: [
						{
							ty: 0,
							nm: "Slider",
							mn: "ADBE Slider Control-0001",
							ix: 1,
							v: {
								a: 0,
								k: 70,
								ix: 1,
								x: "var $bm_rt;\n$bm_rt = thisComp.layer('Waves_Dato_2').effect('Today')('Slider');"
							}
						}
					]
				},
				{
					ty: 5,
					nm: "Counter",
					np: 3,
					mn: "ADBE Slider Control",
					ix: 2,
					en: 1,
					ef: [
						{
							ty: 0,
							nm: "Slider",
							mn: "ADBE Slider Control-0001",
							ix: 1,
							v: {
								a: 1,
								k: [
									{
										i: {
											x: [0.01],
											y: [1]
										},
										o: {
											x: [0.167],
											y: [0.167]
										},
										t: 89,
										s: [70]
									},
									{
										t: 113,
										s: [100]
									}
								],
								ix: 1
							}
						}
					]
				},
				{
					ty: 5,
					nm: "Color",
					np: 3,
					mn: "ADBE Color Control",
					ix: 3,
					en: 1,
					ef: [
						{
							ty: 2,
							nm: "Color",
							mn: "ADBE Color Control-0001",
							ix: 1,
							v: {
								a: 0,
								k: [1, 0, 0, 1],
								ix: 1,
								x: "var $bm_rt;\nvar v = thisLayer.effect('Value')('Slider').value;\nif (v < 50) {\n    $bm_rt = thisComp.layer('Level 1').content('Fill 1').color;\n} else if (v < 70) {\n    $bm_rt = thisComp.layer('Level 2').content('Fill 1').color;\n} else if (v < 100) {\n    $bm_rt = thisComp.layer('Level 3').content('Fill 1').color;\n} else if (v < 150) {\n    $bm_rt = thisComp.layer('Level 4').content('Fill 1').color;\n} else {\n    $bm_rt = thisComp.layer('Level 5').content('Fill 1').color;\n}"
							}
						}
					]
				}
			],
			t: {
				d: {
					x: "var $bm_rt;\n$bm_rt = Math.round($bm_div($bm_mul(effect('Value')('Slider'), effect('Counter')('Slider')), 100));",
					k: [
						{
							s: {
								s: 55,
								f: "Arial-BoldMT",
								t: "8.2",
								j: 1,
								tr: 0,
								lh: 66,
								ls: 0,
								fc: [0.161, 0.8, 0.694]
							},
							t: 0
						}
					]
				},
				p: {},
				m: {
					g: 1,
					a: {
						a: 0,
						k: [0, 0],
						ix: 2
					}
				},
				a: [
					{
						nm: "Color",
						s: {
							t: 0,
							xe: {
								a: 0,
								k: 0,
								ix: 7
							},
							ne: {
								a: 0,
								k: 0,
								ix: 8
							},
							a: {
								a: 0,
								k: 100,
								ix: 4
							},
							b: 1,
							rn: 0,
							sh: 1,
							r: 1
						},
						a: {
							fc: {
								a: 0,
								k: [0.160784319043, 0.800000011921, 0.694117665291, 1],
								ix: 12,
								x: "var $bm_rt;\n$bm_rt = effect('Color')('Color');"
							}
						}
					}
				]
			},
			ip: 89,
			op: 163,
			st: 77,
			bm: 0
		},
		{
			ddd: 0,
			ind: 17,
			ty: 5,
			nm: "VALUE_1",
			sr: 1,
			ks: {
				o: {
					a: 0,
					k: 100,
					ix: 11
				},
				r: {
					a: 0,
					k: 0,
					ix: 10
				},
				p: {
					a: 1,
					k: [
						{
							i: {
								x: 0.01,
								y: 1
							},
							o: {
								x: 0.66,
								y: 0
							},
							t: 27,
							s: [1850.735, 1233.376, 0],
							to: [0, -5, 0],
							ti: [0, 5, 0]
						},
						{
							i: {
								x: 0.01,
								y: 0.01
							},
							o: {
								x: 0.66,
								y: 0.66
							},
							t: 64,
							s: [1850.735, 1203.376, 0],
							to: [0, 0, 0],
							ti: [0, 0, 0]
						},
						{
							i: {
								x: 0.01,
								y: 1
							},
							o: {
								x: 0.66,
								y: 0
							},
							t: 75,
							s: [1850.735, 1203.376, 0],
							to: [0, -13.333, 0],
							ti: [0, 13.333, 0]
						},
						{
							t: 112,
							s: [1850.735, 1123.376, 0]
						}
					],
					ix: 2,
					l: 2
				},
				a: {
					a: 0,
					k: [0, 0, 0],
					ix: 1,
					l: 2
				},
				s: {
					a: 0,
					k: [131.836, 131.836, 100],
					ix: 6,
					l: 2
				}
			},
			ao: 0,
			ef: [
				{
					ty: 5,
					nm: "Value",
					np: 3,
					mn: "ADBE Slider Control",
					ix: 1,
					en: 1,
					ef: [
						{
							ty: 0,
							nm: "Slider",
							mn: "ADBE Slider Control-0001",
							ix: 1,
							v: {
								a: 0,
								k: 8.2,
								ix: 1,
								x: "var $bm_rt;\n$bm_rt = thisComp.layer('Circle_Dato_1').effect('Today')('Slider');"
							}
						}
					]
				},
				{
					ty: 5,
					nm: "Counter",
					np: 3,
					mn: "ADBE Slider Control",
					ix: 2,
					en: 1,
					ef: [
						{
							ty: 0,
							nm: "Slider",
							mn: "ADBE Slider Control-0001",
							ix: 1,
							v: {
								a: 1,
								k: [
									{
										i: {
											x: [0.01],
											y: [1]
										},
										o: {
											x: [0.167],
											y: [0.167]
										},
										t: 41,
										s: [70]
									},
									{
										t: 65,
										s: [100]
									}
								],
								ix: 1
							}
						}
					]
				},
				{
					ty: 5,
					nm: "Color",
					np: 3,
					mn: "ADBE Color Control",
					ix: 3,
					en: 1,
					ef: [
						{
							ty: 2,
							nm: "Color",
							mn: "ADBE Color Control-0001",
							ix: 1,
							v: {
								a: 0,
								k: [1, 0, 0, 1],
								ix: 1,
								x: "var $bm_rt;\nvar v = thisLayer.effect('Value')('Slider').value;\nif (v < 50) {\n    $bm_rt = thisComp.layer('Level 1').content('Fill 1').color;\n} else if (v < 70) {\n    $bm_rt = thisComp.layer('Level 2').content('Fill 1').color;\n} else if (v < 100) {\n    $bm_rt = thisComp.layer('Level 3').content('Fill 1').color;\n} else if (v < 150) {\n    $bm_rt = thisComp.layer('Level 4').content('Fill 1').color;\n} else {\n    $bm_rt = thisComp.layer('Level 5').content('Fill 1').color;\n}"
							}
						}
					]
				}
			],
			t: {
				d: {
					x: "var $bm_rt;\n$bm_rt = Math.round($bm_div($bm_mul(effect('Value')('Slider'), effect('Counter')('Slider')), 100));",
					k: [
						{
							s: {
								s: 55,
								f: "Arial-BoldMT",
								t: "8.2",
								j: 1,
								tr: 0,
								lh: 66,
								ls: 0,
								fc: [0.161, 0.8, 0.694]
							},
							t: 0
						}
					]
				},
				p: {},
				m: {
					g: 1,
					a: {
						a: 0,
						k: [0, 0],
						ix: 2
					}
				},
				a: [
					{
						nm: "Color",
						s: {
							t: 0,
							xe: {
								a: 0,
								k: 0,
								ix: 7
							},
							ne: {
								a: 0,
								k: 0,
								ix: 8
							},
							a: {
								a: 0,
								k: 100,
								ix: 4
							},
							b: 1,
							rn: 0,
							sh: 1,
							r: 1
						},
						a: {
							fc: {
								a: 0,
								k: [0.160784319043, 0.800000011921, 0.694117665291, 1],
								ix: 12,
								x: "var $bm_rt;\n$bm_rt = effect('Color')('Color');"
							}
						}
					}
				]
			},
			ip: 41,
			op: 89,
			st: 29,
			bm: 0
		},
		{
			ddd: 0,
			ind: 18,
			ty: 5,
			nm: "Carbon Monoxide 2",
			sr: 1,
			ks: {
				o: {
					a: 0,
					k: 100,
					ix: 11
				},
				r: {
					a: 0,
					k: 0,
					ix: 10
				},
				p: {
					a: 1,
					k: [
						{
							i: {
								x: 0.01,
								y: 1
							},
							o: {
								x: 0.66,
								y: 0
							},
							t: 150,
							s: [1850.736, 1281.576, 0],
							to: [0, -5, 0],
							ti: [0, 5, 0]
						},
						{
							t: 187,
							s: [1850.736, 1251.576, 0]
						}
					],
					ix: 2,
					l: 2
				},
				a: {
					a: 0,
					k: [0, 0, 0],
					ix: 1,
					l: 2
				},
				s: {
					a: 0,
					k: [131.836, 131.836, 100],
					ix: 6,
					l: 2
				}
			},
			ao: 0,
			t: {
				d: {
					k: [
						{
							s: {
								s: 35,
								f: "Arial-BoldMT",
								t: "PM2.5",
								j: 1,
								tr: 0,
								lh: 42,
								ls: 0,
								fc: [1, 1, 1]
							},
							t: 0
						}
					]
				},
				p: {},
				m: {
					g: 1,
					a: {
						a: 0,
						k: [0, 0],
						ix: 2
					}
				},
				a: []
			},
			ip: 163,
			op: 398,
			st: 148,
			bm: 0
		},
		{
			ddd: 0,
			ind: 19,
			ty: 5,
			nm: "Carbon Monoxide 3",
			sr: 1,
			ks: {
				o: {
					a: 0,
					k: 100,
					ix: 11
				},
				r: {
					a: 0,
					k: 0,
					ix: 10
				},
				p: {
					a: 1,
					k: [
						{
							i: {
								x: 0.01,
								y: 1
							},
							o: {
								x: 0.66,
								y: 0
							},
							t: 77,
							s: [1850.736, 1281.576, 0],
							to: [0, -5, 0],
							ti: [0, 5, 0]
						},
						{
							i: {
								x: 0.01,
								y: 0.01
							},
							o: {
								x: 0.66,
								y: 0.66
							},
							t: 114,
							s: [1850.736, 1251.576, 0],
							to: [0, 0, 0],
							ti: [0, 0, 0]
						},
						{
							i: {
								x: 0.01,
								y: 1
							},
							o: {
								x: 0.66,
								y: 0
							},
							t: 150,
							s: [1850.736, 1251.576, 0],
							to: [0, -5, 0],
							ti: [0, 5, 0]
						},
						{
							t: 187,
							s: [1850.736, 1221.576, 0]
						}
					],
					ix: 2,
					l: 2
				},
				a: {
					a: 0,
					k: [0, 0, 0],
					ix: 1,
					l: 2
				},
				s: {
					a: 0,
					k: [131.836, 131.836, 100],
					ix: 6,
					l: 2
				}
			},
			ao: 0,
			t: {
				d: {
					k: [
						{
							s: {
								s: 35,
								f: "Arial-BoldMT",
								t: "PM10",
								j: 1,
								tr: 0,
								lh: 42,
								ls: 0,
								fc: [1, 1, 1]
							},
							t: 0
						}
					]
				},
				p: {},
				m: {
					g: 1,
					a: {
						a: 0,
						k: [0, 0],
						ix: 2
					}
				},
				a: []
			},
			ip: 89,
			op: 163,
			st: 75,
			bm: 0
		},
		{
			ddd: 0,
			ind: 20,
			ty: 5,
			nm: "Carbon Monoxide",
			sr: 1,
			ks: {
				o: {
					a: 0,
					k: 100,
					ix: 11
				},
				r: {
					a: 0,
					k: 0,
					ix: 10
				},
				p: {
					a: 1,
					k: [
						{
							i: {
								x: 0.01,
								y: 1
							},
							o: {
								x: 0.66,
								y: 0
							},
							t: 25,
							s: [1850.736, 1221.576, 0],
							to: [0, 5, 0],
							ti: [0, -5, 0]
						},
						{
							i: {
								x: 0.01,
								y: 0.01
							},
							o: {
								x: 0.66,
								y: 0.66
							},
							t: 62,
							s: [1850.736, 1251.576, 0],
							to: [0, 0, 0],
							ti: [0, 0, 0]
						},
						{
							i: {
								x: 0.01,
								y: 1
							},
							o: {
								x: 0.66,
								y: 0
							},
							t: 77,
							s: [1850.736, 1251.576, 0],
							to: [0, -5, 0],
							ti: [0, 5, 0]
						},
						{
							t: 114,
							s: [1850.736, 1221.576, 0]
						}
					],
					ix: 2,
					l: 2
				},
				a: {
					a: 0,
					k: [0, 0, 0],
					ix: 1,
					l: 2
				},
				s: {
					a: 0,
					k: [131.836, 131.836, 100],
					ix: 6,
					l: 2
				}
			},
			ao: 0,
			t: {
				d: {
					k: [
						{
							s: {
								s: 35,
								f: "Arial-BoldMT",
								t: "Ozone",
								j: 1,
								tr: 0,
								lh: 42,
								ls: 0,
								fc: [1, 1, 1]
							},
							t: 0
						}
					]
				},
				p: {},
				m: {
					g: 1,
					a: {
						a: 0,
						k: [0, 0],
						ix: 2
					}
				},
				a: []
			},
			ip: 39,
			op: 89,
			st: 25,
			bm: 0
		},
		{
			ddd: 0,
			ind: 21,
			ty: 4,
			nm: "3_Circles",
			sr: 1,
			ks: {
				o: {
					a: 0,
					k: 100,
					ix: 11
				},
				r: {
					a: 1,
					k: [
						{
							i: {
								x: [0.01],
								y: [1]
							},
							o: {
								x: [0.4],
								y: [0]
							},
							t: 152,
							s: [-360]
						},
						{
							t: 202,
							s: [0]
						}
					],
					ix: 10,
					x: "var $bm_rt;\n$bm_rt = $bm_sum(thisComp.layer('Righette_Dato_3').transform.rotation, value);"
				},
				p: {
					a: 0,
					k: [30, 10, 0],
					ix: 2,
					x: "var $bm_rt;\nvar pathLayer = thisComp.layer('Righette_Dato_3');\nvar progress = $bm_div(thisComp.layer('Righette_Dato_3').content('Trim Paths 1').end, 100);\nvar pathToTrace = pathLayer('ADBE Root Vectors Group')(1)('ADBE Vector Shape');\n$bm_rt = $bm_sum(pathLayer.toComp(pathToTrace.pointOnPath(progress)), value);",
					l: 2
				},
				a: {
					a: 0,
					k: [453.49, 290.491, 0],
					ix: 1,
					l: 2
				},
				s: {
					a: 0,
					k: [120, 120, 100],
					ix: 6,
					l: 2
				}
			},
			ao: 0,
			shapes: [
				{
					ty: "gr",
					it: [
						{
							ind: 0,
							ty: "sh",
							ix: 1,
							ks: {
								a: 0,
								k: {
									i: [
										[0, -16.132],
										[16.132, 0],
										[0, 16.132],
										[-16.132, 0]
									],
									o: [
										[0, 16.132],
										[-16.132, 0],
										[0, -16.132],
										[16.132, 0]
									],
									v: [
										[480.331, 249.698],
										[451.122, 278.906],
										[421.914, 249.698],
										[451.122, 220.489]
									],
									c: true
								},
								ix: 2
							},
							nm: "Path 1",
							mn: "ADBE Vector Shape - Group",
							hd: false
						},
						{
							ty: "fl",
							c: {
								a: 0,
								k: [0.945098039216, 0.945098039216, 0.945098039216, 1],
								ix: 4
							},
							o: {
								a: 0,
								k: 100,
								ix: 5
							},
							r: 1,
							bm: 0,
							nm: "Fill 1",
							mn: "ADBE Vector Graphic - Fill",
							hd: false
						},
						{
							ty: "tr",
							p: {
								a: 0,
								k: [0, 0],
								ix: 2
							},
							a: {
								a: 0,
								k: [0, 0],
								ix: 1
							},
							s: {
								a: 0,
								k: [100, 100],
								ix: 3
							},
							r: {
								a: 0,
								k: 0,
								ix: 6
							},
							o: {
								a: 0,
								k: 100,
								ix: 7
							},
							sk: {
								a: 0,
								k: 0,
								ix: 4
							},
							sa: {
								a: 0,
								k: 0,
								ix: 5
							},
							nm: "Transform"
						}
					],
					nm: "Group 1",
					np: 2,
					cix: 2,
					bm: 0,
					ix: 1,
					mn: "ADBE Vector Group",
					hd: false
				},
				{
					ty: "gr",
					it: [
						{
							ind: 0,
							ty: "sh",
							ix: 1,
							ks: {
								a: 0,
								k: {
									i: [
										[0, -16.132],
										[16.132, 0],
										[0, 16.132],
										[-16.132, 0]
									],
									o: [
										[0, 16.132],
										[-16.132, 0],
										[0, -16.132],
										[16.132, 0]
									],
									v: [
										[518.979, 304.058],
										[489.77, 333.267],
										[460.562, 304.058],
										[489.77, 274.849]
									],
									c: true
								},
								ix: 2
							},
							nm: "Path 1",
							mn: "ADBE Vector Shape - Group",
							hd: false
						},
						{
							ty: "fl",
							c: {
								a: 0,
								k: [0.945098039216, 0.945098039216, 0.945098039216, 1],
								ix: 4
							},
							o: {
								a: 0,
								k: 100,
								ix: 5
							},
							r: 1,
							bm: 0,
							nm: "Fill 1",
							mn: "ADBE Vector Graphic - Fill",
							hd: false
						},
						{
							ty: "tr",
							p: {
								a: 0,
								k: [0, 0],
								ix: 2
							},
							a: {
								a: 0,
								k: [0, 0],
								ix: 1
							},
							s: {
								a: 0,
								k: [100, 100],
								ix: 3
							},
							r: {
								a: 0,
								k: 0,
								ix: 6
							},
							o: {
								a: 0,
								k: 100,
								ix: 7
							},
							sk: {
								a: 0,
								k: 0,
								ix: 4
							},
							sa: {
								a: 0,
								k: 0,
								ix: 5
							},
							nm: "Transform"
						}
					],
					nm: "Group 2",
					np: 2,
					cix: 2,
					bm: 0,
					ix: 2,
					mn: "ADBE Vector Group",
					hd: false
				},
				{
					ty: "gr",
					it: [
						{
							ind: 0,
							ty: "sh",
							ix: 1,
							ks: {
								a: 0,
								k: {
									i: [
										[0, -16.684],
										[16.684, 0],
										[0, 16.684],
										[-16.684, 0]
									],
									o: [
										[0, 16.684],
										[-16.684, 0],
										[0, -16.684],
										[16.684, 0]
									],
									v: [
										[450.99, 312.699],
										[420.781, 342.908],
										[390.573, 312.699],
										[420.781, 282.491]
									],
									c: true
								},
								ix: 2
							},
							nm: "Path 1",
							mn: "ADBE Vector Shape - Group",
							hd: false
						},
						{
							ty: "fl",
							c: {
								a: 0,
								k: [0.945098039216, 0.945098039216, 0.945098039216, 1],
								ix: 4
							},
							o: {
								a: 0,
								k: 100,
								ix: 5
							},
							r: 1,
							bm: 0,
							nm: "Fill 1",
							mn: "ADBE Vector Graphic - Fill",
							hd: false
						},
						{
							ty: "tr",
							p: {
								a: 0,
								k: [0, 0],
								ix: 2
							},
							a: {
								a: 0,
								k: [0, 0],
								ix: 1
							},
							s: {
								a: 0,
								k: [100, 100],
								ix: 3
							},
							r: {
								a: 0,
								k: 0,
								ix: 6
							},
							o: {
								a: 0,
								k: 100,
								ix: 7
							},
							sk: {
								a: 0,
								k: 0,
								ix: 4
							},
							sa: {
								a: 0,
								k: 0,
								ix: 5
							},
							nm: "Transform"
						}
					],
					nm: "Group 3",
					np: 2,
					cix: 2,
					bm: 0,
					ix: 3,
					mn: "ADBE Vector Group",
					hd: false
				}
			],
			ip: 152,
			op: 402,
			st: 152,
			bm: 0
		},
		{
			ddd: 0,
			ind: 22,
			ty: 4,
			nm: "Righette_Dato_3",
			sr: 1,
			ks: {
				o: {
					a: 0,
					k: 100,
					ix: 11
				},
				r: {
					a: 1,
					k: [
						{
							i: {
								x: [0.833],
								y: [0.903]
							},
							o: {
								x: [0.167],
								y: [0.167]
							},
							t: 152,
							s: [0.061]
						},
						{
							i: {
								x: [0.833],
								y: [1.275]
							},
							o: {
								x: [0.167],
								y: [0.583]
							},
							t: 153,
							s: [0.128]
						},
						{
							i: {
								x: [0.833],
								y: [0.735]
							},
							o: {
								x: [0.167],
								y: [0.064]
							},
							t: 154,
							s: [0.139]
						},
						{
							i: {
								x: [0.833],
								y: [0.795]
							},
							o: {
								x: [0.167],
								y: [0.122]
							},
							t: 155,
							s: [0.091]
						},
						{
							i: {
								x: [0.833],
								y: [0.815]
							},
							o: {
								x: [0.167],
								y: [0.14]
							},
							t: 156,
							s: [-0.013]
						},
						{
							i: {
								x: [0.833],
								y: [0.828]
							},
							o: {
								x: [0.167],
								y: [0.152]
							},
							t: 157,
							s: [-0.166]
						},
						{
							i: {
								x: [0.833],
								y: [0.839]
							},
							o: {
								x: [0.167],
								y: [0.161]
							},
							t: 158,
							s: [-0.351]
						},
						{
							i: {
								x: [0.833],
								y: [0.854]
							},
							o: {
								x: [0.167],
								y: [0.173]
							},
							t: 159,
							s: [-0.549]
						},
						{
							i: {
								x: [0.833],
								y: [0.851]
							},
							o: {
								x: [0.167],
								y: [0.194]
							},
							t: 160,
							s: [-0.733]
						},
						{
							i: {
								x: [0.833],
								y: [0.845]
							},
							o: {
								x: [0.167],
								y: [0.188]
							},
							t: 161,
							s: [-0.871]
						},
						{
							i: {
								x: [0.833],
								y: [0.854]
							},
							o: {
								x: [0.167],
								y: [0.181]
							},
							t: 162,
							s: [-0.981]
						},
						{
							i: {
								x: [0.833],
								y: [0.868]
							},
							o: {
								x: [0.167],
								y: [0.195]
							},
							t: 163,
							s: [-1.075]
						},
						{
							i: {
								x: [0.833],
								y: [0.897]
							},
							o: {
								x: [0.167],
								y: [0.225]
							},
							t: 164,
							s: [-1.145]
						},
						{
							i: {
								x: [0.833],
								y: [1.093]
							},
							o: {
								x: [0.167],
								y: [0.43]
							},
							t: 165,
							s: [-1.186]
						},
						{
							i: {
								x: [0.833],
								y: [0.723]
							},
							o: {
								x: [0.167],
								y: [0.044]
							},
							t: 166,
							s: [-1.196]
						},
						{
							i: {
								x: [0.833],
								y: [0.796]
							},
							o: {
								x: [0.167],
								y: [0.119]
							},
							t: 167,
							s: [-1.175]
						},
						{
							i: {
								x: [0.833],
								y: [0.818]
							},
							o: {
								x: [0.167],
								y: [0.141]
							},
							t: 168,
							s: [-1.126]
						},
						{
							i: {
								x: [0.833],
								y: [0.832]
							},
							o: {
								x: [0.167],
								y: [0.154]
							},
							t: 169,
							s: [-1.056]
						},
						{
							i: {
								x: [0.833],
								y: [0.847]
							},
							o: {
								x: [0.167],
								y: [0.166]
							},
							t: 170,
							s: [-0.972]
						},
						{
							i: {
								x: [0.833],
								y: [0.869]
							},
							o: {
								x: [0.167],
								y: [0.182]
							},
							t: 171,
							s: [-0.888]
						},
						{
							i: {
								x: [0.833],
								y: [0.921]
							},
							o: {
								x: [0.167],
								y: [0.23]
							},
							t: 172,
							s: [-0.817]
						},
						{
							i: {
								x: [0.833],
								y: [0.211]
							},
							o: {
								x: [0.167],
								y: [-1.489]
							},
							t: 173,
							s: [-0.776]
						},
						{
							i: {
								x: [0.833],
								y: [0.862]
							},
							o: {
								x: [0.167],
								y: [0.093]
							},
							t: 174,
							s: [-0.778]
						},
						{
							i: {
								x: [0.833],
								y: [0.994]
							},
							o: {
								x: [0.167],
								y: [0.211]
							},
							t: 175,
							s: [-0.796]
						},
						{
							i: {
								x: [0.833],
								y: [0.569]
							},
							o: {
								x: [0.167],
								y: [-0.007]
							},
							t: 176,
							s: [-0.808]
						},
						{
							i: {
								x: [0.833],
								y: [0.757]
							},
							o: {
								x: [0.167],
								y: [0.103]
							},
							t: 177,
							s: [-0.797]
						},
						{
							i: {
								x: [0.833],
								y: [0.791]
							},
							o: {
								x: [0.167],
								y: [0.127]
							},
							t: 178,
							s: [-0.752]
						},
						{
							i: {
								x: [0.833],
								y: [0.806]
							},
							o: {
								x: [0.167],
								y: [0.138]
							},
							t: 179,
							s: [-0.664]
						},
						{
							i: {
								x: [0.833],
								y: [0.816]
							},
							o: {
								x: [0.167],
								y: [0.146]
							},
							t: 180,
							s: [-0.531]
						},
						{
							i: {
								x: [0.833],
								y: [0.823]
							},
							o: {
								x: [0.167],
								y: [0.152]
							},
							t: 181,
							s: [-0.356]
						},
						{
							i: {
								x: [0.833],
								y: [0.83]
							},
							o: {
								x: [0.167],
								y: [0.158]
							},
							t: 182,
							s: [-0.144]
						},
						{
							i: {
								x: [0.833],
								y: [0.837]
							},
							o: {
								x: [0.167],
								y: [0.163]
							},
							t: 183,
							s: [0.094]
						},
						{
							i: {
								x: [0.833],
								y: [0.845]
							},
							o: {
								x: [0.167],
								y: [0.17]
							},
							t: 184,
							s: [0.342]
						},
						{
							i: {
								x: [0.833],
								y: [0.85]
							},
							o: {
								x: [0.167],
								y: [0.181]
							},
							t: 185,
							s: [0.58]
						},
						{
							i: {
								x: [0.833],
								y: [0.857]
							},
							o: {
								x: [0.167],
								y: [0.188]
							},
							t: 186,
							s: [0.783]
						},
						{
							i: {
								x: [0.833],
								y: [0.874]
							},
							o: {
								x: [0.167],
								y: [0.2]
							},
							t: 187,
							s: [0.946]
						},
						{
							i: {
								x: [0.833],
								y: [0.92]
							},
							o: {
								x: [0.167],
								y: [0.246]
							},
							t: 188,
							s: [1.062]
						},
						{
							i: {
								x: [0.833],
								y: [-1.106]
							},
							o: {
								x: [0.167],
								y: [-1.763]
							},
							t: 189,
							s: [1.121]
						},
						{
							i: {
								x: [0.833],
								y: [0.759]
							},
							o: {
								x: [0.167],
								y: [0.087]
							},
							t: 190,
							s: [1.119]
						},
						{
							i: {
								x: [0.833],
								y: [0.801]
							},
							o: {
								x: [0.167],
								y: [0.127]
							},
							t: 191,
							s: [1.054]
						},
						{
							i: {
								x: [0.833],
								y: [0.817]
							},
							o: {
								x: [0.167],
								y: [0.143]
							},
							t: 192,
							s: [0.931]
						},
						{
							i: {
								x: [0.833],
								y: [0.827]
							},
							o: {
								x: [0.167],
								y: [0.153]
							},
							t: 193,
							s: [0.761]
						},
						{
							i: {
								x: [0.833],
								y: [0.837]
							},
							o: {
								x: [0.167],
								y: [0.161]
							},
							t: 194,
							s: [0.556]
						},
						{
							i: {
								x: [0.833],
								y: [0.849]
							},
							o: {
								x: [0.167],
								y: [0.17]
							},
							t: 195,
							s: [0.337]
						},
						{
							i: {
								x: [0.833],
								y: [0.868]
							},
							o: {
								x: [0.167],
								y: [0.185]
							},
							t: 196,
							s: [0.127]
						},
						{
							i: {
								x: [0.833],
								y: [0.895]
							},
							o: {
								x: [0.167],
								y: [0.227]
							},
							t: 197,
							s: [-0.045]
						},
						{
							i: {
								x: [0.833],
								y: [0.764]
							},
							o: {
								x: [0.167],
								y: [0.403]
							},
							t: 198,
							s: [-0.145]
						},
						{
							i: {
								x: [0.833],
								y: [0.794]
							},
							o: {
								x: [0.167],
								y: [0.129]
							},
							t: 199,
							s: [-0.171]
						},
						{
							i: {
								x: [0.833],
								y: [0.816]
							},
							o: {
								x: [0.167],
								y: [0.14]
							},
							t: 200,
							s: [-0.218]
						},
						{
							i: {
								x: [0.833],
								y: [0.825]
							},
							o: {
								x: [0.167],
								y: [0.152]
							},
							t: 201,
							s: [-0.288]
						},
						{
							i: {
								x: [0.833],
								y: [0.831]
							},
							o: {
								x: [0.167],
								y: [0.159]
							},
							t: 202,
							s: [-0.373]
						},
						{
							i: {
								x: [0.833],
								y: [0.836]
							},
							o: {
								x: [0.167],
								y: [0.165]
							},
							t: 203,
							s: [-0.465]
						},
						{
							i: {
								x: [0.833],
								y: [0.841]
							},
							o: {
								x: [0.167],
								y: [0.17]
							},
							t: 204,
							s: [-0.56]
						},
						{
							i: {
								x: [0.833],
								y: [0.846]
							},
							o: {
								x: [0.167],
								y: [0.175]
							},
							t: 205,
							s: [-0.652]
						},
						{
							i: {
								x: [0.833],
								y: [0.852]
							},
							o: {
								x: [0.167],
								y: [0.181]
							},
							t: 206,
							s: [-0.735]
						},
						{
							i: {
								x: [0.833],
								y: [0.862]
							},
							o: {
								x: [0.167],
								y: [0.191]
							},
							t: 207,
							s: [-0.806]
						},
						{
							i: {
								x: [0.833],
								y: [0.881]
							},
							o: {
								x: [0.167],
								y: [0.209]
							},
							t: 208,
							s: [-0.862]
						},
						{
							i: {
								x: [0.833],
								y: [0.949]
							},
							o: {
								x: [0.167],
								y: [0.278]
							},
							t: 209,
							s: [-0.898]
						},
						{
							i: {
								x: [0.833],
								y: [0.901]
							},
							o: {
								x: [0.167],
								y: [-0.13]
							},
							t: 210,
							s: [-0.914]
						},
						{
							i: {
								x: [0.833],
								y: [1.132]
							},
							o: {
								x: [0.167],
								y: [0.52]
							},
							t: 211,
							s: [-0.908]
						},
						{
							i: {
								x: [0.833],
								y: [1.196]
							},
							o: {
								x: [0.167],
								y: [0.051]
							},
							t: 212,
							s: [-0.906]
						},
						{
							i: {
								x: [0.833],
								y: [0.617]
							},
							o: {
								x: [0.167],
								y: [0.058]
							},
							t: 213,
							s: [-0.91]
						},
						{
							i: {
								x: [0.833],
								y: [0.753]
							},
							o: {
								x: [0.167],
								y: [0.106]
							},
							t: 214,
							s: [-0.899]
						},
						{
							i: {
								x: [0.833],
								y: [0.787]
							},
							o: {
								x: [0.167],
								y: [0.126]
							},
							t: 215,
							s: [-0.863]
						},
						{
							i: {
								x: [0.833],
								y: [0.803]
							},
							o: {
								x: [0.167],
								y: [0.137]
							},
							t: 216,
							s: [-0.791]
						},
						{
							i: {
								x: [0.833],
								y: [0.813]
							},
							o: {
								x: [0.167],
								y: [0.144]
							},
							t: 217,
							s: [-0.68]
						},
						{
							i: {
								x: [0.833],
								y: [0.82]
							},
							o: {
								x: [0.167],
								y: [0.15]
							},
							t: 218,
							s: [-0.528]
						},
						{
							i: {
								x: [0.833],
								y: [0.827]
							},
							o: {
								x: [0.167],
								y: [0.156]
							},
							t: 219,
							s: [-0.339]
						},
						{
							i: {
								x: [0.833],
								y: [0.833]
							},
							o: {
								x: [0.167],
								y: [0.161]
							},
							t: 220,
							s: [-0.12]
						},
						{
							i: {
								x: [0.833],
								y: [0.84]
							},
							o: {
								x: [0.167],
								y: [0.166]
							},
							t: 221,
							s: [0.115]
						},
						{
							i: {
								x: [0.833],
								y: [0.846]
							},
							o: {
								x: [0.167],
								y: [0.174]
							},
							t: 222,
							s: [0.352]
						},
						{
							i: {
								x: [0.833],
								y: [0.848]
							},
							o: {
								x: [0.167],
								y: [0.182]
							},
							t: 223,
							s: [0.57]
						},
						{
							i: {
								x: [0.833],
								y: [0.866]
							},
							o: {
								x: [0.167],
								y: [0.185]
							},
							t: 224,
							s: [0.754]
						},
						{
							i: {
								x: [0.833],
								y: [0.906]
							},
							o: {
								x: [0.167],
								y: [0.221]
							},
							t: 225,
							s: [0.905]
						},
						{
							i: {
								x: [0.833],
								y: [1.515]
							},
							o: {
								x: [0.167],
								y: [0.76]
							},
							t: 226,
							s: [0.996]
						},
						{
							i: {
								x: [0.833],
								y: [0.735]
							},
							o: {
								x: [0.167],
								y: [0.072]
							},
							t: 227,
							s: [1.007]
						},
						{
							i: {
								x: [0.833],
								y: [0.791]
							},
							o: {
								x: [0.167],
								y: [0.122]
							},
							t: 228,
							s: [0.927]
						},
						{
							i: {
								x: [0.833],
								y: [0.809]
							},
							o: {
								x: [0.167],
								y: [0.138]
							},
							t: 229,
							s: [0.751]
						},
						{
							i: {
								x: [0.833],
								y: [0.82]
							},
							o: {
								x: [0.167],
								y: [0.148]
							},
							t: 230,
							s: [0.486]
						},
						{
							i: {
								x: [0.833],
								y: [0.829]
							},
							o: {
								x: [0.167],
								y: [0.155]
							},
							t: 231,
							s: [0.144]
						},
						{
							i: {
								x: [0.833],
								y: [0.837]
							},
							o: {
								x: [0.167],
								y: [0.162]
							},
							t: 232,
							s: [-0.251]
						},
						{
							i: {
								x: [0.833],
								y: [0.847]
							},
							o: {
								x: [0.167],
								y: [0.17]
							},
							t: 233,
							s: [-0.669]
						},
						{
							i: {
								x: [0.833],
								y: [0.863]
							},
							o: {
								x: [0.167],
								y: [0.183]
							},
							t: 234,
							s: [-1.07]
						},
						{
							i: {
								x: [0.833],
								y: [0.855]
							},
							o: {
								x: [0.167],
								y: [0.212]
							},
							t: 235,
							s: [-1.407]
						},
						{
							i: {
								x: [0.833],
								y: [0.846]
							},
							o: {
								x: [0.167],
								y: [0.196]
							},
							t: 236,
							s: [-1.624]
						},
						{
							i: {
								x: [0.833],
								y: [0.86]
							},
							o: {
								x: [0.167],
								y: [0.181]
							},
							t: 237,
							s: [-1.785]
						},
						{
							i: {
								x: [0.833],
								y: [0.884]
							},
							o: {
								x: [0.167],
								y: [0.205]
							},
							t: 238,
							s: [-1.922]
						},
						{
							i: {
								x: [0.833],
								y: [0.98]
							},
							o: {
								x: [0.167],
								y: [0.296]
							},
							t: 239,
							s: [-2.016]
						},
						{
							i: {
								x: [0.833],
								y: [0.637]
							},
							o: {
								x: [0.167],
								y: [-0.026]
							},
							t: 240,
							s: [-2.052]
						},
						{
							i: {
								x: [0.833],
								y: [0.779]
							},
							o: {
								x: [0.167],
								y: [0.108]
							},
							t: 241,
							s: [-2.024]
						},
						{
							i: {
								x: [0.833],
								y: [0.807]
							},
							o: {
								x: [0.167],
								y: [0.134]
							},
							t: 242,
							s: [-1.931]
						},
						{
							i: {
								x: [0.833],
								y: [0.82]
							},
							o: {
								x: [0.167],
								y: [0.146]
							},
							t: 243,
							s: [-1.776]
						},
						{
							i: {
								x: [0.833],
								y: [0.83]
							},
							o: {
								x: [0.167],
								y: [0.155]
							},
							t: 244,
							s: [-1.572]
						},
						{
							i: {
								x: [0.833],
								y: [0.84]
							},
							o: {
								x: [0.167],
								y: [0.164]
							},
							t: 245,
							s: [-1.336]
						},
						{
							i: {
								x: [0.833],
								y: [0.855]
							},
							o: {
								x: [0.167],
								y: [0.175]
							},
							t: 246,
							s: [-1.091]
						},
						{
							i: {
								x: [0.833],
								y: [0.864]
							},
							o: {
								x: [0.167],
								y: [0.195]
							},
							t: 247,
							s: [-0.867]
						},
						{
							i: {
								x: [0.833],
								y: [0.817]
							},
							o: {
								x: [0.167],
								y: [0.216]
							},
							t: 248,
							s: [-0.7]
						},
						{
							i: {
								x: [0.833],
								y: [0.83]
							},
							o: {
								x: [0.167],
								y: [0.153]
							},
							t: 249,
							s: [-0.595]
						},
						{
							i: {
								x: [0.833],
								y: [0.842]
							},
							o: {
								x: [0.167],
								y: [0.163]
							},
							t: 250,
							s: [-0.47]
						},
						{
							i: {
								x: [0.833],
								y: [0.854]
							},
							o: {
								x: [0.167],
								y: [0.177]
							},
							t: 251,
							s: [-0.339]
						},
						{
							i: {
								x: [0.833],
								y: [0.87]
							},
							o: {
								x: [0.167],
								y: [0.194]
							},
							t: 252,
							s: [-0.222]
						},
						{
							i: {
								x: [0.833],
								y: [0.908]
							},
							o: {
								x: [0.167],
								y: [0.232]
							},
							t: 253,
							s: [-0.134]
						},
						{
							i: {
								x: [0.833],
								y: [1.555]
							},
							o: {
								x: [0.167],
								y: [0.876]
							},
							t: 254,
							s: [-0.084]
						},
						{
							i: {
								x: [0.833],
								y: [0.747]
							},
							o: {
								x: [0.167],
								y: [0.072]
							},
							t: 255,
							s: [-0.079]
						},
						{
							i: {
								x: [0.833],
								y: [0.8]
							},
							o: {
								x: [0.167],
								y: [0.124]
							},
							t: 256,
							s: [-0.119]
						},
						{
							i: {
								x: [0.833],
								y: [0.819]
							},
							o: {
								x: [0.167],
								y: [0.143]
							},
							t: 257,
							s: [-0.2]
						},
						{
							i: {
								x: [0.833],
								y: [0.832]
							},
							o: {
								x: [0.167],
								y: [0.155]
							},
							t: 258,
							s: [-0.313]
						},
						{
							i: {
								x: [0.833],
								y: [0.846]
							},
							o: {
								x: [0.167],
								y: [0.166]
							},
							t: 259,
							s: [-0.446]
						},
						{
							i: {
								x: [0.833],
								y: [0.855]
							},
							o: {
								x: [0.167],
								y: [0.182]
							},
							t: 260,
							s: [-0.581]
						},
						{
							i: {
								x: [0.833],
								y: [0.86]
							},
							o: {
								x: [0.167],
								y: [0.196]
							},
							t: 261,
							s: [-0.694]
						},
						{
							i: {
								x: [0.833],
								y: [0.874]
							},
							o: {
								x: [0.167],
								y: [0.207]
							},
							t: 262,
							s: [-0.778]
						},
						{
							i: {
								x: [0.833],
								y: [0.908]
							},
							o: {
								x: [0.167],
								y: [0.245]
							},
							t: 263,
							s: [-0.834]
						},
						{
							i: {
								x: [0.833],
								y: [1.498]
							},
							o: {
								x: [0.167],
								y: [0.903]
							},
							t: 264,
							s: [-0.864]
						},
						{
							i: {
								x: [0.833],
								y: [0.754]
							},
							o: {
								x: [0.167],
								y: [0.071]
							},
							t: 265,
							s: [-0.867]
						},
						{
							i: {
								x: [0.833],
								y: [0.804]
							},
							o: {
								x: [0.167],
								y: [0.126]
							},
							t: 266,
							s: [-0.846]
						},
						{
							i: {
								x: [0.833],
								y: [0.822]
							},
							o: {
								x: [0.167],
								y: [0.145]
							},
							t: 267,
							s: [-0.805]
						},
						{
							i: {
								x: [0.833],
								y: [0.834]
							},
							o: {
								x: [0.167],
								y: [0.157]
							},
							t: 268,
							s: [-0.75]
						},
						{
							i: {
								x: [0.833],
								y: [0.848]
							},
							o: {
								x: [0.167],
								y: [0.168]
							},
							t: 269,
							s: [-0.688]
						},
						{
							i: {
								x: [0.833],
								y: [0.869]
							},
							o: {
								x: [0.167],
								y: [0.184]
							},
							t: 270,
							s: [-0.627]
						},
						{
							i: {
								x: [0.833],
								y: [0.93]
							},
							o: {
								x: [0.167],
								y: [0.228]
							},
							t: 271,
							s: [-0.576]
						},
						{
							i: {
								x: [0.833],
								y: [0.698]
							},
							o: {
								x: [0.167],
								y: [-0.447]
							},
							t: 272,
							s: [-0.546]
						},
						{
							i: {
								x: [0.833],
								y: [1.523]
							},
							o: {
								x: [0.167],
								y: [0.115]
							},
							t: 273,
							s: [-0.551]
						},
						{
							i: {
								x: [0.833],
								y: [0.761]
							},
							o: {
								x: [0.167],
								y: [0.072]
							},
							t: 274,
							s: [-0.563]
						},
						{
							i: {
								x: [0.833],
								y: [0.811]
							},
							o: {
								x: [0.167],
								y: [0.128]
							},
							t: 275,
							s: [-0.474]
						},
						{
							i: {
								x: [0.833],
								y: [0.828]
							},
							o: {
								x: [0.167],
								y: [0.149]
							},
							t: 276,
							s: [-0.309]
						},
						{
							i: {
								x: [0.833],
								y: [0.839]
							},
							o: {
								x: [0.167],
								y: [0.162]
							},
							t: 277,
							s: [-0.099]
						},
						{
							i: {
								x: [0.833],
								y: [0.85]
							},
							o: {
								x: [0.167],
								y: [0.173]
							},
							t: 278,
							s: [0.124]
						},
						{
							i: {
								x: [0.833],
								y: [0.867]
							},
							o: {
								x: [0.167],
								y: [0.187]
							},
							t: 279,
							s: [0.331]
						},
						{
							i: {
								x: [0.833],
								y: [0.921]
							},
							o: {
								x: [0.167],
								y: [0.223]
							},
							t: 280,
							s: [0.498]
						},
						{
							i: {
								x: [0.833],
								y: [-1.977]
							},
							o: {
								x: [0.167],
								y: [-1.657]
							},
							t: 281,
							s: [0.598]
						},
						{
							i: {
								x: [0.833],
								y: [0.838]
							},
							o: {
								x: [0.167],
								y: [0.086]
							},
							t: 282,
							s: [0.594]
						},
						{
							i: {
								x: [0.833],
								y: [0.863]
							},
							o: {
								x: [0.167],
								y: [0.172]
							},
							t: 283,
							s: [0.427]
						},
						{
							i: {
								x: [0.833],
								y: [0.856]
							},
							o: {
								x: [0.167],
								y: [0.214]
							},
							t: 284,
							s: [0.27]
						},
						{
							i: {
								x: [0.833],
								y: [0.863]
							},
							o: {
								x: [0.167],
								y: [0.197]
							},
							t: 285,
							s: [0.17]
						},
						{
							i: {
								x: [0.833],
								y: [0.871]
							},
							o: {
								x: [0.167],
								y: [0.213]
							},
							t: 286,
							s: [0.096]
						},
						{
							i: {
								x: [0.833],
								y: [0.876]
							},
							o: {
								x: [0.167],
								y: [0.235]
							},
							t: 287,
							s: [0.049]
						},
						{
							i: {
								x: [0.833],
								y: [0.886]
							},
							o: {
								x: [0.167],
								y: [0.256]
							},
							t: 288,
							s: [0.024]
						},
						{
							i: {
								x: [0.833],
								y: [0.903]
							},
							o: {
								x: [0.167],
								y: [0.306]
							},
							t: 289,
							s: [0.011]
						},
						{
							i: {
								x: [0.833],
								y: [0.969]
							},
							o: {
								x: [0.167],
								y: [0.584]
							},
							t: 290,
							s: [0.006]
						},
						{
							i: {
								x: [0.833],
								y: [0.878]
							},
							o: {
								x: [0.167],
								y: [-0.049]
							},
							t: 291,
							s: [0.006]
						},
						{
							i: {
								x: [0.833],
								y: [1.164]
							},
							o: {
								x: [0.167],
								y: [0.263]
							},
							t: 292,
							s: [0.006]
						},
						{
							i: {
								x: [0.833],
								y: [0.728]
							},
							o: {
								x: [0.167],
								y: [0.055]
							},
							t: 293,
							s: [0.006]
						},
						{
							i: {
								x: [0.833],
								y: [0.814]
							},
							o: {
								x: [0.167],
								y: [0.12]
							},
							t: 294,
							s: [0.006]
						},
						{
							i: {
								x: [0.833],
								y: [0.848]
							},
							o: {
								x: [0.167],
								y: [0.151]
							},
							t: 295,
							s: [0.004]
						},
						{
							i: {
								x: [0.833],
								y: [0.884]
							},
							o: {
								x: [0.167],
								y: [0.185]
							},
							t: 296,
							s: [0.002]
						},
						{
							i: {
								x: [0.833],
								y: [0.917]
							},
							o: {
								x: [0.167],
								y: [0.298]
							},
							t: 297,
							s: [0.001]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 298,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 299,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 300,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 301,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 302,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 303,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 304,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 305,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 306,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 307,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 308,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 309,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 310,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 311,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 312,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 313,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 314,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 315,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 316,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 317,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 318,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 319,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 320,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 321,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 322,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 323,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 324,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 325,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 326,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 327,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 328,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 329,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 330,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 331,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 332,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 333,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 334,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 335,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 336,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 337,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 338,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 339,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 340,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 341,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 342,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 343,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 344,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 345,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 346,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 347,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 348,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 349,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 350,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 351,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 352,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 353,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 354,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 355,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 356,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 357,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 358,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 359,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 360,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 361,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 362,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 363,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 364,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 365,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 366,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 367,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 368,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 369,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 370,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 371,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 372,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 373,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 374,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 375,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 376,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 377,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 378,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 379,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 380,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 381,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 382,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 383,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 384,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 385,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 386,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 387,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 388,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 389,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 390,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 391,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 392,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 393,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 394,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 395,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 396,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 397,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 398,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 399,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 400,
							s: [0]
						},
						{
							t: 401,
							s: [0]
						}
					],
					ix: 10,
					x: "var $bm_rt;\nvar deltaIndex;\ndeltaIndex = Math.max(Math.min($bm_sub(effect('Today')('Slider'), effect('Forecast')('Slider')), 50), -50);\n$bm_rt = $bm_sum($bm_div($bm_mul(Math.asin($bm_div($bm_div($bm_mul(600, deltaIndex), 50), 1754)), 180), Math.PI), value);"
				},
				p: {
					a: 0,
					k: [1770.655, 1022.689, 0],
					ix: 2,
					l: 2
				},
				a: {
					a: 0,
					k: [420.655, 127.689, 0],
					ix: 1,
					l: 2
				},
				s: {
					a: 0,
					k: [100, 100, 100],
					ix: 6,
					l: 2
				}
			},
			ao: 0,
			ef: [
				{
					ty: 5,
					nm: "Today",
					np: 3,
					mn: "ADBE Slider Control",
					ix: 1,
					en: 1,
					ef: [
						{
							ty: 0,
							nm: "Slider",
							mn: "ADBE Slider Control-0001",
							ix: 1,
							v: {
								a: 0,
								k: 120,
								ix: 1
							}
						}
					]
				},
				{
					ty: 5,
					nm: "Forecast",
					np: 3,
					mn: "ADBE Slider Control",
					ix: 2,
					en: 1,
					ef: [
						{
							ty: 0,
							nm: "Slider",
							mn: "ADBE Slider Control-0001",
							ix: 1,
							v: {
								a: 0,
								k: 116,
								ix: 1
							}
						}
					]
				},
				{
					ty: 5,
					nm: "WiggleAmp",
					np: 3,
					mn: "ADBE Slider Control",
					ix: 3,
					en: 1,
					ef: [
						{
							ty: 0,
							nm: "Slider",
							mn: "ADBE Slider Control-0001",
							ix: 1,
							v: {
								a: 1,
								k: [
									{
										i: {
											x: [0.01],
											y: [1]
										},
										o: {
											x: [0.66],
											y: [0]
										},
										t: 274,
										s: [2]
									},
									{
										t: 298,
										s: [0]
									}
								],
								ix: 1
							}
						}
					]
				}
			],
			shapes: [
				{
					ind: 0,
					ty: "sh",
					ix: 1,
					ks: {
						a: 0,
						k: {
							i: [
								[0, 0],
								[0, 0]
							],
							o: [
								[0, 0],
								[0, 0]
							],
							v: [
								[-660, 113],
								[400, 113]
							],
							c: false
						},
						ix: 2
					},
					nm: "Path 1",
					mn: "ADBE Vector Shape - Group",
					hd: false
				},
				{
					ty: "tm",
					s: {
						a: 0,
						k: 0,
						ix: 1
					},
					e: {
						a: 1,
						k: [
							{
								i: {
									x: [0.01],
									y: [1]
								},
								o: {
									x: [0.4],
									y: [0]
								},
								t: 152,
								s: [0]
							},
							{
								t: 202,
								s: [100]
							}
						],
						ix: 2
					},
					o: {
						a: 0,
						k: 0,
						ix: 3
					},
					m: 1,
					ix: 2,
					nm: "Trim Paths 1",
					mn: "ADBE Vector Filter - Trim",
					hd: false
				},
				{
					ty: "st",
					c: {
						a: 0,
						k: [0.917647058824, 0.321568627451, 0.058823529412, 1],
						ix: 3,
						x: "var $bm_rt;\n$bm_rt = thisComp.layer('VALUE_3').effect('Color')('Color');"
					},
					o: {
						a: 0,
						k: 100,
						ix: 4
					},
					w: {
						a: 0,
						k: 150,
						ix: 5
					},
					lc: 1,
					lj: 1,
					ml: 4,
					bm: 0,
					d: [
						{
							n: "d",
							nm: "dash",
							v: {
								a: 0,
								k: 15,
								ix: 1
							}
						},
						{
							n: "g",
							nm: "gap",
							v: {
								a: 0,
								k: 10,
								ix: 2,
								x: "var $bm_rt;\n$bm_rt = linear(effect('Today')('Slider'), 50, 150, 100, 10);"
							}
						},
						{
							n: "o",
							nm: "offset",
							v: {
								a: 0,
								k: 0,
								ix: 7
							}
						}
					],
					nm: "Stroke 1",
					mn: "ADBE Vector Graphic - Stroke",
					hd: false
				}
			],
			ip: 152,
			op: 402,
			st: 152,
			bm: 0
		},
		{
			ddd: 0,
			ind: 23,
			ty: 4,
			nm: "Circle_Dato_2",
			parent: 24,
			sr: 1,
			ks: {
				o: {
					a: 0,
					k: 100,
					ix: 11
				},
				r: {
					a: 0,
					k: 8.972,
					ix: 10
				},
				p: {
					a: 0,
					k: [386.166, 110.696, 0],
					ix: 2,
					l: 2
				},
				a: {
					a: 0,
					k: [0, 0, 0],
					ix: 1,
					l: 2
				},
				s: {
					a: 0,
					k: [100, 100, 100],
					ix: 6,
					l: 2
				}
			},
			ao: 0,
			shapes: [
				{
					ty: "gr",
					it: [
						{
							d: 1,
							ty: "el",
							s: {
								a: 0,
								k: [120, 120],
								ix: 2
							},
							p: {
								a: 0,
								k: [0, 0],
								ix: 3
							},
							nm: "Ellipse Path 1",
							mn: "ADBE Vector Shape - Ellipse",
							hd: false
						},
						{
							ty: "fl",
							c: {
								a: 0,
								k: [0.945098039216, 0.945098039216, 0.945098039216, 1],
								ix: 4
							},
							o: {
								a: 0,
								k: 100,
								ix: 5
							},
							r: 1,
							bm: 0,
							nm: "Fill 1",
							mn: "ADBE Vector Graphic - Fill",
							hd: false
						},
						{
							ty: "tr",
							p: {
								a: 0,
								k: [0, 0],
								ix: 2
							},
							a: {
								a: 0,
								k: [0, 0],
								ix: 1
							},
							s: {
								a: 0,
								k: [100, 100],
								ix: 3
							},
							r: {
								a: 0,
								k: 0,
								ix: 6
							},
							o: {
								a: 0,
								k: 100,
								ix: 7
							},
							sk: {
								a: 0,
								k: 0,
								ix: 4
							},
							sa: {
								a: 0,
								k: 0,
								ix: 5
							},
							nm: "Transform"
						}
					],
					nm: "Ellipse 1",
					np: 3,
					cix: 2,
					bm: 0,
					ix: 1,
					mn: "ADBE Vector Group",
					hd: false
				}
			],
			ip: 25,
			op: 275,
			st: 25,
			bm: 0
		},
		{
			ddd: 0,
			ind: 24,
			ty: 4,
			nm: "Waves_Dato_2",
			sr: 1,
			ks: {
				o: {
					a: 0,
					k: 100,
					ix: 11
				},
				r: {
					a: 1,
					k: [
						{
							i: {
								x: [0.833],
								y: [0.85]
							},
							o: {
								x: [0.167],
								y: [0.167]
							},
							t: 77,
							s: [0.042]
						},
						{
							i: {
								x: [0.833],
								y: [0.868]
							},
							o: {
								x: [0.167],
								y: [0.188]
							},
							t: 78,
							s: [0.295]
						},
						{
							i: {
								x: [0.833],
								y: [0.881]
							},
							o: {
								x: [0.167],
								y: [0.225]
							},
							t: 79,
							s: [0.496]
						},
						{
							i: {
								x: [0.833],
								y: [0.932]
							},
							o: {
								x: [0.167],
								y: [0.278]
							},
							t: 80,
							s: [0.615]
						},
						{
							i: {
								x: [0.833],
								y: [0.374]
							},
							o: {
								x: [0.167],
								y: [-0.368]
							},
							t: 81,
							s: [0.666]
						},
						{
							i: {
								x: [0.833],
								y: [0.776]
							},
							o: {
								x: [0.167],
								y: [0.096]
							},
							t: 82,
							s: [0.656]
						},
						{
							i: {
								x: [0.833],
								y: [0.809]
							},
							o: {
								x: [0.167],
								y: [0.133]
							},
							t: 83,
							s: [0.595]
						},
						{
							i: {
								x: [0.833],
								y: [0.822]
							},
							o: {
								x: [0.167],
								y: [0.148]
							},
							t: 84,
							s: [0.493]
						},
						{
							i: {
								x: [0.833],
								y: [0.831]
							},
							o: {
								x: [0.167],
								y: [0.157]
							},
							t: 85,
							s: [0.36]
						},
						{
							i: {
								x: [0.833],
								y: [0.839]
							},
							o: {
								x: [0.167],
								y: [0.164]
							},
							t: 86,
							s: [0.209]
						},
						{
							i: {
								x: [0.833],
								y: [0.849]
							},
							o: {
								x: [0.167],
								y: [0.173]
							},
							t: 87,
							s: [0.054]
						},
						{
							i: {
								x: [0.833],
								y: [0.866]
							},
							o: {
								x: [0.167],
								y: [0.187]
							},
							t: 88,
							s: [-0.09]
						},
						{
							i: {
								x: [0.833],
								y: [0.91]
							},
							o: {
								x: [0.167],
								y: [0.22]
							},
							t: 89,
							s: [-0.206]
						},
						{
							i: {
								x: [0.833],
								y: [2.089]
							},
							o: {
								x: [0.167],
								y: [1.097]
							},
							t: 90,
							s: [-0.277]
						},
						{
							i: {
								x: [0.833],
								y: [0.722]
							},
							o: {
								x: [0.167],
								y: [0.077]
							},
							t: 91,
							s: [-0.283]
						},
						{
							i: {
								x: [0.833],
								y: [0.801]
							},
							o: {
								x: [0.167],
								y: [0.119]
							},
							t: 92,
							s: [-0.201]
						},
						{
							i: {
								x: [0.833],
								y: [0.823]
							},
							o: {
								x: [0.167],
								y: [0.143]
							},
							t: 93,
							s: [-0.011]
						},
						{
							i: {
								x: [0.833],
								y: [0.833]
							},
							o: {
								x: [0.167],
								y: [0.157]
							},
							t: 94,
							s: [0.253]
						},
						{
							i: {
								x: [0.833],
								y: [0.841]
							},
							o: {
								x: [0.167],
								y: [0.167]
							},
							t: 95,
							s: [0.55]
						},
						{
							i: {
								x: [0.833],
								y: [0.848]
							},
							o: {
								x: [0.167],
								y: [0.175]
							},
							t: 96,
							s: [0.847]
						},
						{
							i: {
								x: [0.833],
								y: [0.857]
							},
							o: {
								x: [0.167],
								y: [0.185]
							},
							t: 97,
							s: [1.118]
						},
						{
							i: {
								x: [0.833],
								y: [0.87]
							},
							o: {
								x: [0.167],
								y: [0.199]
							},
							t: 98,
							s: [1.34]
						},
						{
							i: {
								x: [0.833],
								y: [0.901]
							},
							o: {
								x: [0.167],
								y: [0.233]
							},
							t: 99,
							s: [1.5]
						},
						{
							i: {
								x: [0.833],
								y: [1.175]
							},
							o: {
								x: [0.167],
								y: [0.529]
							},
							t: 100,
							s: [1.589]
						},
						{
							i: {
								x: [0.833],
								y: [0.74]
							},
							o: {
								x: [0.167],
								y: [0.056]
							},
							t: 101,
							s: [1.606]
						},
						{
							i: {
								x: [0.833],
								y: [0.802]
							},
							o: {
								x: [0.167],
								y: [0.123]
							},
							t: 102,
							s: [1.554]
						},
						{
							i: {
								x: [0.833],
								y: [0.792]
							},
							o: {
								x: [0.167],
								y: [0.144]
							},
							t: 103,
							s: [1.445]
						},
						{
							i: {
								x: [0.833],
								y: [0.788]
							},
							o: {
								x: [0.167],
								y: [0.139]
							},
							t: 104,
							s: [1.295]
						},
						{
							i: {
								x: [0.833],
								y: [0.815]
							},
							o: {
								x: [0.167],
								y: [0.137]
							},
							t: 105,
							s: [1.071]
						},
						{
							i: {
								x: [0.833],
								y: [0.827]
							},
							o: {
								x: [0.167],
								y: [0.152]
							},
							t: 106,
							s: [0.727]
						},
						{
							i: {
								x: [0.833],
								y: [0.834]
							},
							o: {
								x: [0.167],
								y: [0.161]
							},
							t: 107,
							s: [0.308]
						},
						{
							i: {
								x: [0.833],
								y: [0.839]
							},
							o: {
								x: [0.167],
								y: [0.167]
							},
							t: 108,
							s: [-0.143]
						},
						{
							i: {
								x: [0.833],
								y: [0.844]
							},
							o: {
								x: [0.167],
								y: [0.173]
							},
							t: 109,
							s: [-0.592]
						},
						{
							i: {
								x: [0.833],
								y: [0.85]
							},
							o: {
								x: [0.167],
								y: [0.179]
							},
							t: 110,
							s: [-1.008]
						},
						{
							i: {
								x: [0.833],
								y: [0.856]
							},
							o: {
								x: [0.167],
								y: [0.187]
							},
							t: 111,
							s: [-1.371]
						},
						{
							i: {
								x: [0.833],
								y: [0.867]
							},
							o: {
								x: [0.167],
								y: [0.198]
							},
							t: 112,
							s: [-1.662]
						},
						{
							i: {
								x: [0.833],
								y: [0.887]
							},
							o: {
								x: [0.167],
								y: [0.222]
							},
							t: 113,
							s: [-1.874]
						},
						{
							i: {
								x: [0.833],
								y: [0.968]
							},
							o: {
								x: [0.167],
								y: [0.317]
							},
							t: 114,
							s: [-2]
						},
						{
							i: {
								x: [0.833],
								y: [0.657]
							},
							o: {
								x: [0.167],
								y: [-0.053]
							},
							t: 115,
							s: [-2.046]
						},
						{
							i: {
								x: [0.833],
								y: [0.786]
							},
							o: {
								x: [0.167],
								y: [0.11]
							},
							t: 116,
							s: [-2.018]
						},
						{
							i: {
								x: [0.833],
								y: [0.81]
							},
							o: {
								x: [0.167],
								y: [0.137]
							},
							t: 117,
							s: [-1.932]
						},
						{
							i: {
								x: [0.833],
								y: [0.82]
							},
							o: {
								x: [0.167],
								y: [0.148]
							},
							t: 118,
							s: [-1.798]
						},
						{
							i: {
								x: [0.833],
								y: [0.826]
							},
							o: {
								x: [0.167],
								y: [0.155]
							},
							t: 119,
							s: [-1.626]
						},
						{
							i: {
								x: [0.833],
								y: [0.83]
							},
							o: {
								x: [0.167],
								y: [0.16]
							},
							t: 120,
							s: [-1.426]
						},
						{
							i: {
								x: [0.833],
								y: [0.834]
							},
							o: {
								x: [0.167],
								y: [0.164]
							},
							t: 121,
							s: [-1.209]
						},
						{
							i: {
								x: [0.833],
								y: [0.837]
							},
							o: {
								x: [0.167],
								y: [0.167]
							},
							t: 122,
							s: [-0.985]
						},
						{
							i: {
								x: [0.833],
								y: [0.839]
							},
							o: {
								x: [0.167],
								y: [0.17]
							},
							t: 123,
							s: [-0.761]
						},
						{
							i: {
								x: [0.833],
								y: [0.842]
							},
							o: {
								x: [0.167],
								y: [0.173]
							},
							t: 124,
							s: [-0.545]
						},
						{
							i: {
								x: [0.833],
								y: [0.846]
							},
							o: {
								x: [0.167],
								y: [0.177]
							},
							t: 125,
							s: [-0.346]
						},
						{
							i: {
								x: [0.833],
								y: [0.851]
							},
							o: {
								x: [0.167],
								y: [0.182]
							},
							t: 126,
							s: [-0.168]
						},
						{
							i: {
								x: [0.833],
								y: [0.858]
							},
							o: {
								x: [0.167],
								y: [0.189]
							},
							t: 127,
							s: [-0.017]
						},
						{
							i: {
								x: [0.833],
								y: [0.852]
							},
							o: {
								x: [0.167],
								y: [0.202]
							},
							t: 128,
							s: [0.102]
						},
						{
							i: {
								x: [0.833],
								y: [0.83]
							},
							o: {
								x: [0.167],
								y: [0.191]
							},
							t: 129,
							s: [0.185]
						},
						{
							i: {
								x: [0.833],
								y: [0.847]
							},
							o: {
								x: [0.167],
								y: [0.163]
							},
							t: 130,
							s: [0.25]
						},
						{
							i: {
								x: [0.833],
								y: [0.866]
							},
							o: {
								x: [0.167],
								y: [0.183]
							},
							t: 131,
							s: [0.317]
						},
						{
							i: {
								x: [0.833],
								y: [0.905]
							},
							o: {
								x: [0.167],
								y: [0.22]
							},
							t: 132,
							s: [0.373]
						},
						{
							i: {
								x: [0.833],
								y: [1.401]
							},
							o: {
								x: [0.167],
								y: [0.667]
							},
							t: 133,
							s: [0.408]
						},
						{
							i: {
								x: [0.833],
								y: [0.734]
							},
							o: {
								x: [0.167],
								y: [0.069]
							},
							t: 134,
							s: [0.412]
						},
						{
							i: {
								x: [0.833],
								y: [0.792]
							},
							o: {
								x: [0.167],
								y: [0.121]
							},
							t: 135,
							s: [0.384]
						},
						{
							i: {
								x: [0.833],
								y: [0.811]
							},
							o: {
								x: [0.167],
								y: [0.139]
							},
							t: 136,
							s: [0.322]
						},
						{
							i: {
								x: [0.833],
								y: [0.823]
							},
							o: {
								x: [0.167],
								y: [0.149]
							},
							t: 137,
							s: [0.229]
						},
						{
							i: {
								x: [0.833],
								y: [0.832]
							},
							o: {
								x: [0.167],
								y: [0.157]
							},
							t: 138,
							s: [0.111]
						},
						{
							i: {
								x: [0.833],
								y: [0.843]
							},
							o: {
								x: [0.167],
								y: [0.166]
							},
							t: 139,
							s: [-0.022]
						},
						{
							i: {
								x: [0.833],
								y: [0.864]
							},
							o: {
								x: [0.167],
								y: [0.177]
							},
							t: 140,
							s: [-0.156]
						},
						{
							i: {
								x: [0.833],
								y: [0.991]
							},
							o: {
								x: [0.167],
								y: [0.216]
							},
							t: 141,
							s: [-0.275]
						},
						{
							i: {
								x: [0.833],
								y: [0.673]
							},
							o: {
								x: [0.167],
								y: [-0.011]
							},
							t: 142,
							s: [-0.351]
						},
						{
							i: {
								x: [0.833],
								y: [0.795]
							},
							o: {
								x: [0.167],
								y: [0.112]
							},
							t: 143,
							s: [-0.284]
						},
						{
							i: {
								x: [0.833],
								y: [0.818]
							},
							o: {
								x: [0.167],
								y: [0.141]
							},
							t: 144,
							s: [-0.088]
						},
						{
							i: {
								x: [0.833],
								y: [0.829]
							},
							o: {
								x: [0.167],
								y: [0.154]
							},
							t: 145,
							s: [0.196]
						},
						{
							i: {
								x: [0.833],
								y: [0.836]
							},
							o: {
								x: [0.167],
								y: [0.162]
							},
							t: 146,
							s: [0.532]
						},
						{
							i: {
								x: [0.833],
								y: [0.842]
							},
							o: {
								x: [0.167],
								y: [0.169]
							},
							t: 147,
							s: [0.888]
						},
						{
							i: {
								x: [0.833],
								y: [0.849]
							},
							o: {
								x: [0.167],
								y: [0.177]
							},
							t: 148,
							s: [1.233]
						},
						{
							i: {
								x: [0.833],
								y: [0.86]
							},
							o: {
								x: [0.167],
								y: [0.186]
							},
							t: 149,
							s: [1.541]
						},
						{
							i: {
								x: [0.833],
								y: [0.879]
							},
							o: {
								x: [0.167],
								y: [0.205]
							},
							t: 150,
							s: [1.79]
						},
						{
							i: {
								x: [0.833],
								y: [0.95]
							},
							o: {
								x: [0.167],
								y: [0.269]
							},
							t: 151,
							s: [1.961]
						},
						{
							i: {
								x: [0.833],
								y: [0.514]
							},
							o: {
								x: [0.167],
								y: [-0.128]
							},
							t: 152,
							s: [2.038]
						},
						{
							i: {
								x: [0.833],
								y: [0.755]
							},
							o: {
								x: [0.167],
								y: [0.101]
							},
							t: 153,
							s: [2.007]
						},
						{
							i: {
								x: [0.833],
								y: [0.796]
							},
							o: {
								x: [0.167],
								y: [0.126]
							},
							t: 154,
							s: [1.862]
						},
						{
							i: {
								x: [0.833],
								y: [0.82]
							},
							o: {
								x: [0.167],
								y: [0.141]
							},
							t: 155,
							s: [1.579]
						},
						{
							i: {
								x: [0.833],
								y: [0.831]
							},
							o: {
								x: [0.167],
								y: [0.155]
							},
							t: 156,
							s: [1.17]
						},
						{
							i: {
								x: [0.833],
								y: [0.838]
							},
							o: {
								x: [0.167],
								y: [0.164]
							},
							t: 157,
							s: [0.695]
						},
						{
							i: {
								x: [0.833],
								y: [0.844]
							},
							o: {
								x: [0.167],
								y: [0.171]
							},
							t: 158,
							s: [0.203]
						},
						{
							i: {
								x: [0.833],
								y: [0.85]
							},
							o: {
								x: [0.167],
								y: [0.179]
							},
							t: 159,
							s: [-0.262]
						},
						{
							i: {
								x: [0.833],
								y: [0.858]
							},
							o: {
								x: [0.167],
								y: [0.188]
							},
							t: 160,
							s: [-0.67]
						},
						{
							i: {
								x: [0.833],
								y: [0.87]
							},
							o: {
								x: [0.167],
								y: [0.201]
							},
							t: 161,
							s: [-0.996]
						},
						{
							i: {
								x: [0.833],
								y: [0.896]
							},
							o: {
								x: [0.167],
								y: [0.232]
							},
							t: 162,
							s: [-1.226]
						},
						{
							i: {
								x: [0.833],
								y: [1.058]
							},
							o: {
								x: [0.167],
								y: [0.426]
							},
							t: 163,
							s: [-1.354]
						},
						{
							i: {
								x: [0.833],
								y: [0.736]
							},
							o: {
								x: [0.167],
								y: [0.034]
							},
							t: 164,
							s: [-1.386]
						},
						{
							i: {
								x: [0.833],
								y: [0.81]
							},
							o: {
								x: [0.167],
								y: [0.122]
							},
							t: 165,
							s: [-1.332]
						},
						{
							i: {
								x: [0.833],
								y: [0.823]
							},
							o: {
								x: [0.167],
								y: [0.148]
							},
							t: 166,
							s: [-1.217]
						},
						{
							i: {
								x: [0.833],
								y: [0.829]
							},
							o: {
								x: [0.167],
								y: [0.157]
							},
							t: 167,
							s: [-1.069]
						},
						{
							i: {
								x: [0.833],
								y: [0.833]
							},
							o: {
								x: [0.167],
								y: [0.162]
							},
							t: 168,
							s: [-0.902]
						},
						{
							i: {
								x: [0.833],
								y: [0.837]
							},
							o: {
								x: [0.167],
								y: [0.167]
							},
							t: 169,
							s: [-0.725]
						},
						{
							i: {
								x: [0.833],
								y: [0.84]
							},
							o: {
								x: [0.167],
								y: [0.17]
							},
							t: 170,
							s: [-0.549]
						},
						{
							i: {
								x: [0.833],
								y: [0.842]
							},
							o: {
								x: [0.167],
								y: [0.174]
							},
							t: 171,
							s: [-0.38]
						},
						{
							i: {
								x: [0.833],
								y: [0.845]
							},
							o: {
								x: [0.167],
								y: [0.177]
							},
							t: 172,
							s: [-0.224]
						},
						{
							i: {
								x: [0.833],
								y: [0.846]
							},
							o: {
								x: [0.167],
								y: [0.18]
							},
							t: 173,
							s: [-0.085]
						},
						{
							i: {
								x: [0.833],
								y: [0.847]
							},
							o: {
								x: [0.167],
								y: [0.182]
							},
							t: 174,
							s: [0.034]
						},
						{
							i: {
								x: [0.833],
								y: [0.846]
							},
							o: {
								x: [0.167],
								y: [0.183]
							},
							t: 175,
							s: [0.136]
						},
						{
							i: {
								x: [0.833],
								y: [0.841]
							},
							o: {
								x: [0.167],
								y: [0.181]
							},
							t: 176,
							s: [0.22]
						},
						{
							i: {
								x: [0.833],
								y: [0.832]
							},
							o: {
								x: [0.167],
								y: [0.175]
							},
							t: 177,
							s: [0.292]
						},
						{
							i: {
								x: [0.833],
								y: [0.832]
							},
							o: {
								x: [0.167],
								y: [0.165]
							},
							t: 178,
							s: [0.357]
						},
						{
							i: {
								x: [0.833],
								y: [0.857]
							},
							o: {
								x: [0.167],
								y: [0.166]
							},
							t: 179,
							s: [0.423]
						},
						{
							i: {
								x: [0.833],
								y: [0.886]
							},
							o: {
								x: [0.167],
								y: [0.2]
							},
							t: 180,
							s: [0.49]
						},
						{
							i: {
								x: [0.833],
								y: [1.015]
							},
							o: {
								x: [0.167],
								y: [0.312]
							},
							t: 181,
							s: [0.537]
						},
						{
							i: {
								x: [0.833],
								y: [0.664]
							},
							o: {
								x: [0.167],
								y: [0.013]
							},
							t: 182,
							s: [0.555]
						},
						{
							i: {
								x: [0.833],
								y: [0.777]
							},
							o: {
								x: [0.167],
								y: [0.111]
							},
							t: 183,
							s: [0.534]
						},
						{
							i: {
								x: [0.833],
								y: [0.803]
							},
							o: {
								x: [0.167],
								y: [0.133]
							},
							t: 184,
							s: [0.472]
						},
						{
							i: {
								x: [0.833],
								y: [0.815]
							},
							o: {
								x: [0.167],
								y: [0.144]
							},
							t: 185,
							s: [0.368]
						},
						{
							i: {
								x: [0.833],
								y: [0.824]
							},
							o: {
								x: [0.167],
								y: [0.152]
							},
							t: 186,
							s: [0.226]
						},
						{
							i: {
								x: [0.833],
								y: [0.831]
							},
							o: {
								x: [0.167],
								y: [0.158]
							},
							t: 187,
							s: [0.053]
						},
						{
							i: {
								x: [0.833],
								y: [0.839]
							},
							o: {
								x: [0.167],
								y: [0.165]
							},
							t: 188,
							s: [-0.14]
						},
						{
							i: {
								x: [0.833],
								y: [0.849]
							},
							o: {
								x: [0.167],
								y: [0.173]
							},
							t: 189,
							s: [-0.338]
						},
						{
							i: {
								x: [0.833],
								y: [0.862]
							},
							o: {
								x: [0.167],
								y: [0.186]
							},
							t: 190,
							s: [-0.523]
						},
						{
							i: {
								x: [0.833],
								y: [0.817]
							},
							o: {
								x: [0.167],
								y: [0.211]
							},
							t: 191,
							s: [-0.672]
						},
						{
							i: {
								x: [0.833],
								y: [0.824]
							},
							o: {
								x: [0.167],
								y: [0.153]
							},
							t: 192,
							s: [-0.77]
						},
						{
							i: {
								x: [0.833],
								y: [0.843]
							},
							o: {
								x: [0.167],
								y: [0.159]
							},
							t: 193,
							s: [-0.887]
						},
						{
							i: {
								x: [0.833],
								y: [0.86]
							},
							o: {
								x: [0.167],
								y: [0.178]
							},
							t: 194,
							s: [-1.016]
						},
						{
							i: {
								x: [0.833],
								y: [0.888]
							},
							o: {
								x: [0.167],
								y: [0.206]
							},
							t: 195,
							s: [-1.131]
						},
						{
							i: {
								x: [0.833],
								y: [1.02]
							},
							o: {
								x: [0.167],
								y: [0.327]
							},
							t: 196,
							s: [-1.209]
						},
						{
							i: {
								x: [0.833],
								y: [0.678]
							},
							o: {
								x: [0.167],
								y: [0.016]
							},
							t: 197,
							s: [-1.235]
						},
						{
							i: {
								x: [0.833],
								y: [0.783]
							},
							o: {
								x: [0.167],
								y: [0.112]
							},
							t: 198,
							s: [-1.202]
						},
						{
							i: {
								x: [0.833],
								y: [0.808]
							},
							o: {
								x: [0.167],
								y: [0.135]
							},
							t: 199,
							s: [-1.107]
						},
						{
							i: {
								x: [0.833],
								y: [0.821]
							},
							o: {
								x: [0.167],
								y: [0.147]
							},
							t: 200,
							s: [-0.955]
						},
						{
							i: {
								x: [0.833],
								y: [0.831]
							},
							o: {
								x: [0.167],
								y: [0.156]
							},
							t: 201,
							s: [-0.755]
						},
						{
							i: {
								x: [0.833],
								y: [0.842]
							},
							o: {
								x: [0.167],
								y: [0.164]
							},
							t: 202,
							s: [-0.526]
						},
						{
							i: {
								x: [0.833],
								y: [0.843]
							},
							o: {
								x: [0.167],
								y: [0.176]
							},
							t: 203,
							s: [-0.291]
						},
						{
							i: {
								x: [0.833],
								y: [0.831]
							},
							o: {
								x: [0.167],
								y: [0.177]
							},
							t: 204,
							s: [-0.079]
						},
						{
							i: {
								x: [0.833],
								y: [0.838]
							},
							o: {
								x: [0.167],
								y: [0.164]
							},
							t: 205,
							s: [0.108]
						},
						{
							i: {
								x: [0.833],
								y: [0.843]
							},
							o: {
								x: [0.167],
								y: [0.171]
							},
							t: 206,
							s: [0.301]
						},
						{
							i: {
								x: [0.833],
								y: [0.849]
							},
							o: {
								x: [0.167],
								y: [0.178]
							},
							t: 207,
							s: [0.484]
						},
						{
							i: {
								x: [0.833],
								y: [0.856]
							},
							o: {
								x: [0.167],
								y: [0.186]
							},
							t: 208,
							s: [0.645]
						},
						{
							i: {
								x: [0.833],
								y: [0.866]
							},
							o: {
								x: [0.167],
								y: [0.198]
							},
							t: 209,
							s: [0.775]
						},
						{
							i: {
								x: [0.833],
								y: [0.883]
							},
							o: {
								x: [0.167],
								y: [0.22]
							},
							t: 210,
							s: [0.869]
						},
						{
							i: {
								x: [0.833],
								y: [0.933]
							},
							o: {
								x: [0.167],
								y: [0.29]
							},
							t: 211,
							s: [0.926]
						},
						{
							i: {
								x: [0.833],
								y: [0.518]
							},
							o: {
								x: [0.167],
								y: [-0.331]
							},
							t: 212,
							s: [0.949]
						},
						{
							i: {
								x: [0.833],
								y: [0.819]
							},
							o: {
								x: [0.167],
								y: [0.101]
							},
							t: 213,
							s: [0.944]
						},
						{
							i: {
								x: [0.833],
								y: [0.878]
							},
							o: {
								x: [0.167],
								y: [0.155]
							},
							t: 214,
							s: [0.922]
						},
						{
							i: {
								x: [0.833],
								y: [0.998]
							},
							o: {
								x: [0.167],
								y: [0.263]
							},
							t: 215,
							s: [0.896]
						},
						{
							i: {
								x: [0.833],
								y: [1.328]
							},
							o: {
								x: [0.167],
								y: [-0.002]
							},
							t: 216,
							s: [0.884]
						},
						{
							i: {
								x: [0.833],
								y: [0.728]
							},
							o: {
								x: [0.167],
								y: [0.066]
							},
							t: 217,
							s: [0.896]
						},
						{
							i: {
								x: [0.833],
								y: [0.803]
							},
							o: {
								x: [0.167],
								y: [0.12]
							},
							t: 218,
							s: [0.838]
						},
						{
							i: {
								x: [0.833],
								y: [0.822]
							},
							o: {
								x: [0.167],
								y: [0.145]
							},
							t: 219,
							s: [0.707]
						},
						{
							i: {
								x: [0.833],
								y: [0.832]
							},
							o: {
								x: [0.167],
								y: [0.157]
							},
							t: 220,
							s: [0.529]
						},
						{
							i: {
								x: [0.833],
								y: [0.839]
							},
							o: {
								x: [0.167],
								y: [0.165]
							},
							t: 221,
							s: [0.328]
						},
						{
							i: {
								x: [0.833],
								y: [0.846]
							},
							o: {
								x: [0.167],
								y: [0.173]
							},
							t: 222,
							s: [0.123]
						},
						{
							i: {
								x: [0.833],
								y: [0.854]
							},
							o: {
								x: [0.167],
								y: [0.182]
							},
							t: 223,
							s: [-0.068]
						},
						{
							i: {
								x: [0.833],
								y: [0.868]
							},
							o: {
								x: [0.167],
								y: [0.195]
							},
							t: 224,
							s: [-0.23]
						},
						{
							i: {
								x: [0.833],
								y: [0.9]
							},
							o: {
								x: [0.167],
								y: [0.226]
							},
							t: 225,
							s: [-0.35]
						},
						{
							i: {
								x: [0.833],
								y: [1.18]
							},
							o: {
								x: [0.167],
								y: [0.493]
							},
							t: 226,
							s: [-0.421]
						},
						{
							i: {
								x: [0.833],
								y: [0.722]
							},
							o: {
								x: [0.167],
								y: [0.057]
							},
							t: 227,
							s: [-0.436]
						},
						{
							i: {
								x: [0.833],
								y: [0.765]
							},
							o: {
								x: [0.167],
								y: [0.119]
							},
							t: 228,
							s: [-0.39]
						},
						{
							i: {
								x: [0.833],
								y: [0.79]
							},
							o: {
								x: [0.167],
								y: [0.129]
							},
							t: 229,
							s: [-0.284]
						},
						{
							i: {
								x: [0.833],
								y: [0.817]
							},
							o: {
								x: [0.167],
								y: [0.138]
							},
							t: 230,
							s: [-0.092]
						},
						{
							i: {
								x: [0.833],
								y: [0.829]
							},
							o: {
								x: [0.167],
								y: [0.153]
							},
							t: 231,
							s: [0.2]
						},
						{
							i: {
								x: [0.833],
								y: [0.836]
							},
							o: {
								x: [0.167],
								y: [0.163]
							},
							t: 232,
							s: [0.549]
						},
						{
							i: {
								x: [0.833],
								y: [0.842]
							},
							o: {
								x: [0.167],
								y: [0.17]
							},
							t: 233,
							s: [0.916]
						},
						{
							i: {
								x: [0.833],
								y: [0.849]
							},
							o: {
								x: [0.167],
								y: [0.177]
							},
							t: 234,
							s: [1.269]
						},
						{
							i: {
								x: [0.833],
								y: [0.856]
							},
							o: {
								x: [0.167],
								y: [0.185]
							},
							t: 235,
							s: [1.583]
						},
						{
							i: {
								x: [0.833],
								y: [0.867]
							},
							o: {
								x: [0.167],
								y: [0.198]
							},
							t: 236,
							s: [1.84]
						},
						{
							i: {
								x: [0.833],
								y: [0.889]
							},
							o: {
								x: [0.167],
								y: [0.223]
							},
							t: 237,
							s: [2.027]
						},
						{
							i: {
								x: [0.833],
								y: [0.986]
							},
							o: {
								x: [0.167],
								y: [0.336]
							},
							t: 238,
							s: [2.139]
						},
						{
							i: {
								x: [0.833],
								y: [0.687]
							},
							o: {
								x: [0.167],
								y: [-0.017]
							},
							t: 239,
							s: [2.175]
						},
						{
							i: {
								x: [0.833],
								y: [0.804]
							},
							o: {
								x: [0.167],
								y: [0.114]
							},
							t: 240,
							s: [2.145]
						},
						{
							i: {
								x: [0.833],
								y: [0.852]
							},
							o: {
								x: [0.167],
								y: [0.145]
							},
							t: 241,
							s: [2.06]
						},
						{
							i: {
								x: [0.833],
								y: [0.853]
							},
							o: {
								x: [0.167],
								y: [0.19]
							},
							t: 242,
							s: [1.947]
						},
						{
							i: {
								x: [0.833],
								y: [0.839]
							},
							o: {
								x: [0.167],
								y: [0.192]
							},
							t: 243,
							s: [1.858]
						},
						{
							i: {
								x: [0.833],
								y: [0.823]
							},
							o: {
								x: [0.167],
								y: [0.173]
							},
							t: 244,
							s: [1.79]
						},
						{
							i: {
								x: [0.833],
								y: [0.812]
							},
							o: {
								x: [0.167],
								y: [0.157]
							},
							t: 245,
							s: [1.727]
						},
						{
							i: {
								x: [0.833],
								y: [0.81]
							},
							o: {
								x: [0.167],
								y: [0.15]
							},
							t: 246,
							s: [1.656]
						},
						{
							i: {
								x: [0.833],
								y: [0.812]
							},
							o: {
								x: [0.167],
								y: [0.148]
							},
							t: 247,
							s: [1.566]
						},
						{
							i: {
								x: [0.833],
								y: [0.816]
							},
							o: {
								x: [0.167],
								y: [0.15]
							},
							t: 248,
							s: [1.452]
						},
						{
							i: {
								x: [0.833],
								y: [0.82]
							},
							o: {
								x: [0.167],
								y: [0.152]
							},
							t: 249,
							s: [1.308]
						},
						{
							i: {
								x: [0.833],
								y: [0.824]
							},
							o: {
								x: [0.167],
								y: [0.155]
							},
							t: 250,
							s: [1.134]
						},
						{
							i: {
								x: [0.833],
								y: [0.828]
							},
							o: {
								x: [0.167],
								y: [0.158]
							},
							t: 251,
							s: [0.931]
						},
						{
							i: {
								x: [0.833],
								y: [0.832]
							},
							o: {
								x: [0.167],
								y: [0.161]
							},
							t: 252,
							s: [0.706]
						},
						{
							i: {
								x: [0.833],
								y: [0.847]
							},
							o: {
								x: [0.167],
								y: [0.165]
							},
							t: 253,
							s: [0.464]
						},
						{
							i: {
								x: [0.833],
								y: [0.872]
							},
							o: {
								x: [0.167],
								y: [0.184]
							},
							t: 254,
							s: [0.218]
						},
						{
							i: {
								x: [0.833],
								y: [0.9]
							},
							o: {
								x: [0.167],
								y: [0.239]
							},
							t: 255,
							s: [0.014]
						},
						{
							i: {
								x: [0.833],
								y: [1.132]
							},
							o: {
								x: [0.167],
								y: [0.506]
							},
							t: 256,
							s: [-0.096]
						},
						{
							i: {
								x: [0.833],
								y: [0.737]
							},
							o: {
								x: [0.167],
								y: [0.051]
							},
							t: 257,
							s: [-0.117]
						},
						{
							i: {
								x: [0.833],
								y: [0.799]
							},
							o: {
								x: [0.167],
								y: [0.122]
							},
							t: 258,
							s: [-0.061]
						},
						{
							i: {
								x: [0.833],
								y: [0.818]
							},
							o: {
								x: [0.167],
								y: [0.143]
							},
							t: 259,
							s: [0.059]
						},
						{
							i: {
								x: [0.833],
								y: [0.829]
							},
							o: {
								x: [0.167],
								y: [0.154]
							},
							t: 260,
							s: [0.228]
						},
						{
							i: {
								x: [0.833],
								y: [0.838]
							},
							o: {
								x: [0.167],
								y: [0.162]
							},
							t: 261,
							s: [0.428]
						},
						{
							i: {
								x: [0.833],
								y: [0.849]
							},
							o: {
								x: [0.167],
								y: [0.172]
							},
							t: 262,
							s: [0.639]
						},
						{
							i: {
								x: [0.833],
								y: [0.867]
							},
							o: {
								x: [0.167],
								y: [0.186]
							},
							t: 263,
							s: [0.839]
						},
						{
							i: {
								x: [0.833],
								y: [0.915]
							},
							o: {
								x: [0.167],
								y: [0.222]
							},
							t: 264,
							s: [1]
						},
						{
							i: {
								x: [0.833],
								y: [5.975]
							},
							o: {
								x: [0.167],
								y: [4.296]
							},
							t: 265,
							s: [1.097]
						},
						{
							i: {
								x: [0.833],
								y: [0.805]
							},
							o: {
								x: [0.167],
								y: [0.082]
							},
							t: 266,
							s: [1.099]
						},
						{
							i: {
								x: [0.833],
								y: [0.832]
							},
							o: {
								x: [0.167],
								y: [0.145]
							},
							t: 267,
							s: [0.983]
						},
						{
							i: {
								x: [0.833],
								y: [0.839]
							},
							o: {
								x: [0.167],
								y: [0.166]
							},
							t: 268,
							s: [0.827]
						},
						{
							i: {
								x: [0.833],
								y: [0.844]
							},
							o: {
								x: [0.167],
								y: [0.172]
							},
							t: 269,
							s: [0.668]
						},
						{
							i: {
								x: [0.833],
								y: [0.85]
							},
							o: {
								x: [0.167],
								y: [0.179]
							},
							t: 270,
							s: [0.52]
						},
						{
							i: {
								x: [0.833],
								y: [0.857]
							},
							o: {
								x: [0.167],
								y: [0.188]
							},
							t: 271,
							s: [0.391]
						},
						{
							i: {
								x: [0.833],
								y: [0.867]
							},
							o: {
								x: [0.167],
								y: [0.2]
							},
							t: 272,
							s: [0.289]
						},
						{
							i: {
								x: [0.833],
								y: [0.884]
							},
							o: {
								x: [0.167],
								y: [0.223]
							},
							t: 273,
							s: [0.216]
						},
						{
							i: {
								x: [0.833],
								y: [0.935]
							},
							o: {
								x: [0.167],
								y: [0.299]
							},
							t: 274,
							s: [0.172]
						},
						{
							i: {
								x: [0.833],
								y: [0.581]
							},
							o: {
								x: [0.167],
								y: [-0.299]
							},
							t: 275,
							s: [0.156]
						},
						{
							i: {
								x: [0.833],
								y: [0.84]
							},
							o: {
								x: [0.167],
								y: [0.104]
							},
							t: 276,
							s: [0.159]
						},
						{
							i: {
								x: [0.833],
								y: [0.934]
							},
							o: {
								x: [0.167],
								y: [0.174]
							},
							t: 277,
							s: [0.174]
						},
						{
							i: {
								x: [0.833],
								y: [-0.693]
							},
							o: {
								x: [0.167],
								y: [-0.314]
							},
							t: 278,
							s: [0.188]
						},
						{
							i: {
								x: [0.833],
								y: [0.733]
							},
							o: {
								x: [0.167],
								y: [0.088]
							},
							t: 279,
							s: [0.185]
						},
						{
							i: {
								x: [0.833],
								y: [0.815]
							},
							o: {
								x: [0.167],
								y: [0.121]
							},
							t: 280,
							s: [0.13]
						},
						{
							i: {
								x: [0.833],
								y: [0.847]
							},
							o: {
								x: [0.167],
								y: [0.152]
							},
							t: 281,
							s: [0.008]
						},
						{
							i: {
								x: [0.833],
								y: [0.907]
							},
							o: {
								x: [0.167],
								y: [0.183]
							},
							t: 282,
							s: [-0.14]
						},
						{
							i: {
								x: [0.833],
								y: [1.134]
							},
							o: {
								x: [0.167],
								y: [0.785]
							},
							t: 283,
							s: [-0.264]
						},
						{
							i: {
								x: [0.833],
								y: [0.841]
							},
							o: {
								x: [0.167],
								y: [0.051]
							},
							t: 284,
							s: [-0.278]
						},
						{
							i: {
								x: [0.833],
								y: [0.831]
							},
							o: {
								x: [0.167],
								y: [0.176]
							},
							t: 285,
							s: [-0.24]
						},
						{
							i: {
								x: [0.833],
								y: [0.833]
							},
							o: {
								x: [0.167],
								y: [0.165]
							},
							t: 286,
							s: [-0.205]
						},
						{
							i: {
								x: [0.833],
								y: [0.838]
							},
							o: {
								x: [0.167],
								y: [0.167]
							},
							t: 287,
							s: [-0.17]
						},
						{
							i: {
								x: [0.833],
								y: [0.843]
							},
							o: {
								x: [0.167],
								y: [0.172]
							},
							t: 288,
							s: [-0.134]
						},
						{
							i: {
								x: [0.833],
								y: [0.848]
							},
							o: {
								x: [0.167],
								y: [0.178]
							},
							t: 289,
							s: [-0.101]
						},
						{
							i: {
								x: [0.833],
								y: [0.853]
							},
							o: {
								x: [0.167],
								y: [0.185]
							},
							t: 290,
							s: [-0.071]
						},
						{
							i: {
								x: [0.833],
								y: [0.859]
							},
							o: {
								x: [0.167],
								y: [0.193]
							},
							t: 291,
							s: [-0.047]
						},
						{
							i: {
								x: [0.833],
								y: [0.865]
							},
							o: {
								x: [0.167],
								y: [0.204]
							},
							t: 292,
							s: [-0.028]
						},
						{
							i: {
								x: [0.833],
								y: [0.871]
							},
							o: {
								x: [0.167],
								y: [0.217]
							},
							t: 293,
							s: [-0.015]
						},
						{
							i: {
								x: [0.833],
								y: [0.878]
							},
							o: {
								x: [0.167],
								y: [0.234]
							},
							t: 294,
							s: [-0.007]
						},
						{
							i: {
								x: [0.833],
								y: [0.888]
							},
							o: {
								x: [0.167],
								y: [0.262]
							},
							t: 295,
							s: [-0.003]
						},
						{
							i: {
								x: [0.833],
								y: [0.903]
							},
							o: {
								x: [0.167],
								y: [0.322]
							},
							t: 296,
							s: [-0.001]
						},
						{
							i: {
								x: [0.833],
								y: [0.929]
							},
							o: {
								x: [0.167],
								y: [0.578]
							},
							t: 297,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [0.917]
							},
							o: {
								x: [0.167],
								y: [-0.498]
							},
							t: 298,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 299,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 300,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 301,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 302,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 303,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 304,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 305,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 306,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 307,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 308,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 309,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 310,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 311,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 312,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 313,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 314,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 315,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 316,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 317,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 318,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 319,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 320,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 321,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 322,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 323,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 324,
							s: [0]
						},
						{
							i: {
								x: [0.833],
								y: [1]
							},
							o: {
								x: [0.167],
								y: [0]
							},
							t: 325,
							s: [0]
						},
						{
							t: 326,
							s: [0]
						}
					],
					ix: 10,
					x: "var $bm_rt;\nvar deltaIndex;\ndeltaIndex = Math.max(Math.min($bm_sub(effect('Today')('Slider'), effect('Forecast')('Slider')), 50), -50);\n$bm_rt = $bm_sum($bm_div($bm_mul(Math.asin($bm_div($bm_div($bm_mul(600, deltaIndex), 50), 1754)), 180), Math.PI), value);"
				},
				p: {
					s: true,
					x: {
						a: 1,
						k: [
							{
								i: {
									x: [0.01],
									y: [1]
								},
								o: {
									x: [0.167],
									y: [0.167]
								},
								t: 77,
								s: [780]
							},
							{
								t: 127,
								s: [1690]
							}
						],
						ix: 3
					},
					y: {
						a: 1,
						k: [
							{
								i: {
									x: [0.01],
									y: [1]
								},
								o: {
									x: [0.66],
									y: [0]
								},
								t: 150,
								s: [808]
							},
							{
								t: 174,
								s: [688]
							}
						],
						ix: 4
					}
				},
				a: {
					a: 0,
					k: [440, 113, 0],
					ix: 1,
					l: 2
				},
				s: {
					a: 0,
					k: [100, 100, 100],
					ix: 6,
					l: 2
				}
			},
			ao: 0,
			ef: [
				{
					ty: 5,
					nm: "Today",
					np: 3,
					mn: "ADBE Slider Control",
					ix: 1,
					en: 1,
					ef: [
						{
							ty: 0,
							nm: "Slider",
							mn: "ADBE Slider Control-0001",
							ix: 1,
							v: {
								a: 0,
								k: 70,
								ix: 1
							}
						}
					]
				},
				{
					ty: 5,
					nm: "Forecast",
					np: 3,
					mn: "ADBE Slider Control",
					ix: 2,
					en: 1,
					ef: [
						{
							ty: 0,
							nm: "Slider",
							mn: "ADBE Slider Control-0001",
							ix: 1,
							v: {
								a: 0,
								k: 90,
								ix: 1
							}
						}
					]
				},
				{
					ty: 5,
					nm: "WiggleAmp",
					np: 3,
					mn: "ADBE Slider Control",
					ix: 3,
					en: 1,
					ef: [
						{
							ty: 0,
							nm: "Slider",
							mn: "ADBE Slider Control-0001",
							ix: 1,
							v: {
								a: 1,
								k: [
									{
										i: {
											x: [0.01],
											y: [1]
										},
										o: {
											x: [0.66],
											y: [0]
										},
										t: 275,
										s: [2]
									},
									{
										t: 299,
										s: [0]
									}
								],
								ix: 1
							}
						}
					]
				}
			],
			shapes: [
				{
					ty: "gr",
					it: [
						{
							ty: "gr",
							it: [
								{
									ind: 0,
									ty: "sh",
									ix: 1,
									ks: {
										a: 0,
										k: {
											i: [
												[0, 0],
												[-172.674, 0],
												[-172.695, 0],
												[-172.677, 0],
												[-172.692, 0],
												[-172.68, 0],
												[-172.691, 0],
												[-172.68, 0],
												[-172.696, 0],
												[-172.682, 0],
												[-172.693, 0],
												[-172.687, 0],
												[-172.687, 0],
												[-172.693, 0],
												[-172.689, 0],
												[-172.689, 0],
												[-172.696, 0],
												[-172.688, 0],
												[-172.693, 0],
												[-172.695, 0],
												[-172.698, 0],
												[-172.694, 0],
												[-172.698, 0],
												[-172.698, 0],
												[-172.694, 0],
												[-172.693, 0],
												[-172.697, 0],
												[-172.695, 0],
												[-172.701, 0],
												[-172.682, 0],
												[-172.701, 0],
												[-172.68, 0],
												[-172.682, 0],
												[-172.686, 0],
												[-172.686, 0],
												[-172.691, 0],
												[-172.711, 0],
												[-172.693, 0],
												[-172.703, 0],
												[-172.697, 0],
												[-172.703, 0],
												[-172.703, 0],
												[-172.72, 0],
												[-172.712, 0],
												[-172.701, 0],
												[-172.692, 0],
												[-172.712, 0],
												[-172.718, 0],
												[-172.721, 0],
												[-172.717, 0],
												[-172.744, 0],
												[-43.135, -47.362]
											],
											o: [
												[172.679, 0],
												[172.695, 0],
												[172.676, 0],
												[172.692, 0],
												[172.679, 0],
												[172.691, 0],
												[172.68, 0],
												[172.696, 0],
												[172.68, 0],
												[172.694, 0],
												[172.683, 0],
												[172.687, 0],
												[172.695, 0],
												[172.686, 0],
												[172.686, 0],
												[172.698, 0],
												[172.693, 0],
												[172.682, 0],
												[172.689, 0],
												[172.693, 0],
												[172.696, 0],
												[172.702, 0],
												[172.701, 0],
												[172.698, 0],
												[172.697, 0],
												[172.691, 0],
												[172.693, 0],
												[172.701, 0],
												[172.682, 0],
												[172.697, 0],
												[172.682, 0],
												[172.68, 0],
												[172.684, 0],
												[172.686, 0],
												[172.689, 0],
												[172.709, 0],
												[172.689, 0],
												[172.703, 0],
												[172.695, 0],
												[172.701, 0],
												[172.701, 0],
												[172.711, 0],
												[172.714, 0],
												[172.688, 0],
												[172.688, 0],
												[172.716, 0],
												[172.718, 0],
												[172.724, 0],
												[172.721, 0],
												[172.744, 0],
												[86.264, 0],
												[5.1, 5.599]
											],
											v: [
												[-16413.16, -96.236],
												[-16067.808, 93.441],
												[-15722.417, -96.236],
												[-15377.064, 93.441],
												[-15031.68, -96.236],
												[-14686.322, 93.441],
												[-14340.939, -96.236],
												[-13995.58, 93.441],
												[-13650.188, -96.236],
												[-13304.826, 93.441],
												[-12959.438, -96.236],
												[-12614.071, 93.441],
												[-12268.698, -96.236],
												[-11923.31, 93.441],
												[-11577.937, -96.236],
												[-11232.562, 93.441],
												[-10887.167, -96.236],
												[-10541.784, 93.441],
												[-10196.409, -96.236],
												[-9851.03, 93.441],
												[-9505.643, -96.236],
												[-9160.253, 93.441],
												[-8814.853, -96.236],
												[-8469.452, 93.441],
												[-8124.059, -96.236],
												[-7778.666, 93.441],
												[-7433.279, -96.236],
												[-7087.891, 93.441],
												[-6742.488, -96.236],
												[-6397.125, 93.441],
												[-6051.727, -96.236],
												[-5706.361, 93.441],
												[-5361.004, -96.236],
												[-5015.643, 93.441],
												[-4670.271, -96.236],
												[-4324.891, 93.441],
												[-3979.471, -96.236],
												[-3634.088, 93.441],
												[-3288.682, -96.236],
												[-2943.291, 93.441],
												[-2597.891, -96.236],
												[-2252.486, 93.441],
												[-1907.058, -96.236],
												[-1561.632, 93.441],
												[-1216.247, -96.236],
												[-870.866, 93.441],
												[-525.438, -96.236],
												[-180.003, 93.441],
												[165.443, -96.236],
												[510.883, 93.441],
												[856.371, -96.236],
												[1028.955, -1.572]
											],
											c: false
										},
										ix: 2
									},
									nm: "Path 1",
									mn: "ADBE Vector Shape - Group",
									hd: false
								},
								{
									ty: "tm",
									s: {
										a: 1,
										k: [
											{
												i: {
													x: [0.34],
													y: [1]
												},
												o: {
													x: [0.66],
													y: [0]
												},
												t: 77,
												s: [100]
											},
											{
												t: 127,
												s: [0]
											}
										],
										ix: 1
									},
									e: {
										a: 0,
										k: 100,
										ix: 2
									},
									o: {
										a: 0,
										k: 0,
										ix: 3
									},
									m: 1,
									ix: 2,
									nm: "Trim Paths 1",
									mn: "ADBE Vector Filter - Trim",
									hd: false
								},
								{
									ty: "tr",
									p: {
										a: 0,
										k: [487.768, 644.751],
										ix: 2
									},
									a: {
										a: 0,
										k: [1026.583, -0.249],
										ix: 1
									},
									s: {
										a: 0,
										k: [135, 100],
										ix: 3,
										x: "var $bm_rt;\n$bm_rt = $bm_sum([\n    easeOut(effect('Today')('Slider') * 1.2, 50, 150, 300 / 4, 5),\n    100\n], [\n    content('Group 2').content('Group 1').content('Trim Paths 1').start.valueAtTime(time - 4 / 25) / 4,\n    0\n]);"
									},
									r: {
										a: 0,
										k: 0,
										ix: 6
									},
									o: {
										a: 0,
										k: 100,
										ix: 7
									},
									sk: {
										a: 0,
										k: 0,
										ix: 4
									},
									sa: {
										a: 0,
										k: 0,
										ix: 5
									},
									nm: "Transform"
								}
							],
							nm: "Group 1",
							np: 2,
							cix: 2,
							bm: 0,
							ix: 1,
							mn: "ADBE Vector Group",
							hd: false
						},
						{
							ty: "st",
							c: {
								a: 0,
								k: [0.988235294118, 0.933333333333, 0.427450980392, 1],
								ix: 3,
								x: "var $bm_rt;\n$bm_rt = thisComp.layer('VALUE_2').effect('Color')('Color');"
							},
							o: {
								a: 0,
								k: 100,
								ix: 4
							},
							w: {
								a: 0,
								k: 9,
								ix: 5
							},
							lc: 1,
							lj: 1,
							ml: 4,
							bm: 0,
							nm: "Stroke 1",
							mn: "ADBE Vector Graphic - Stroke",
							hd: false
						},
						{
							ty: "tr",
							p: {
								a: 0,
								k: [-517.816, 105],
								ix: 2
							},
							a: {
								a: 0,
								k: [-402.816, 645],
								ix: 1
							},
							s: {
								a: 0,
								k: [100, 100],
								ix: 3
							},
							r: {
								a: 0,
								k: 0,
								ix: 6
							},
							o: {
								a: 0,
								k: 100,
								ix: 7
							},
							sk: {
								a: 0,
								k: 0,
								ix: 4
							},
							sa: {
								a: 0,
								k: 0,
								ix: 5
							},
							nm: "Transform"
						}
					],
					nm: "Group 2",
					np: 2,
					cix: 2,
					bm: 0,
					ix: 1,
					mn: "ADBE Vector Group",
					hd: false
				}
			],
			ip: 77,
			op: 327,
			st: 77,
			bm: 0
		},
		{
			ddd: 0,
			ind: 25,
			ty: 4,
			nm: "Circle_Dato_1",
			sr: 1,
			ks: {
				o: {
					a: 0,
					k: 100,
					ix: 11
				},
				r: {
					a: 1,
					k: [
						{
							i: {
								x: [0.833],
								y: [1.947]
							},
							o: {
								x: [0.167],
								y: [0.167]
							},
							t: 25,
							s: [-0.473]
						},
						{
							i: {
								x: [0.833],
								y: [0.787]
							},
							o: {
								x: [0.167],
								y: [0.077]
							},
							t: 26,
							s: [-0.471]
						},
						{
							i: {
								x: [0.833],
								y: [0.829]
							},
							o: {
								x: [0.167],
								y: [0.137]
							},
							t: 27,
							s: [-0.503]
						},
						{
							i: {
								x: [0.833],
								y: [0.852]
							},
							o: {
								x: [0.167],
								y: [0.163]
							},
							t: 28,
							s: [-0.553]
						},
						{
							i: {
								x: [0.833],
								y: [0.884]
							},
							o: {
								x: [0.167],
								y: [0.191]
							},
							t: 29,
							s: [-0.606]
						},
						{
							i: {
								x: [0.833],
								y: [1.027]
							},
							o: {
								x: [0.167],
								y: [0.297]
							},
							t: 30,
							s: [-0.647]
						},
						{
							i: {
								x: [0.833],
								y: [0.644]
							},
							o: {
								x: [0.167],
								y: [0.02]
							},
							t: 31,
							s: [-0.663]
						},
						{
							i: {
								x: [0.833],
								y: [0.764]
							},
							o: {
								x: [0.167],
								y: [0.109]
							},
							t: 32,
							s: [-0.642]
						},
						{
							i: {
								x: [0.833],
								y: [0.79]
							},
							o: {
								x: [0.167],
								y: [0.129]
							},
							t: 33,
							s: [-0.573]
						},
						{
							i: {
								x: [0.833],
								y: [0.807]
							},
							o: {
								x: [0.167],
								y: [0.138]
							},
							t: 34,
							s: [-0.446]
						},
						{
							i: {
								x: [0.833],
								y: [0.828]
							},
							o: {
								x: [0.167],
								y: [0.147]
							},
							t: 35,
							s: [-0.254]
						},
						{
							i: {
								x: [0.833],
								y: [0.837]
							},
							o: {
								x: [0.167],
								y: [0.162]
							},
							t: 36,
							s: [-0.002]
						},
						{
							i: {
								x: [0.833],
								y: [0.844]
							},
							o: {
								x: [0.167],
								y: [0.171]
							},
							t: 37,
							s: [0.267]
						},
						{
							i: {
								x: [0.833],
								y: [0.852]
							},
							o: {
								x: [0.167],
								y: [0.179]
							},
							t: 38,
							s: [0.524]
						},
						{
							i: {
								x: [0.833],
								y: [0.861]
							},
							o: {
								x: [0.167],
								y: [0.19]
							},
							t: 39,
							s: [0.748]
						},
						{
							i: {
								x: [0.833],
								y: [0.877]
							},
							o: {
								x: [0.167],
								y: [0.208]
							},
							t: 40,
							s: [0.922]
						},
						{
							i: {
								x: [0.833],
								y: [0.92]
							},
							o: {
								x: [0.167],
								y: [0.258]
							},
							t: 41,
							s: [1.039]
						},
						{
							i: {
								x: [0.833],
								y: [-0.979]
							},
							o: {
								x: [0.167],
								y: [-2.004]
							},
							t: 42,
							s: [1.094]
						},
						{
							i: {
								x: [0.833],
								y: [0.78]
							},
							o: {
								x: [0.167],
								y: [0.087]
							},
							t: 43,
							s: [1.092]
						},
						{
							i: {
								x: [0.833],
								y: [0.823]
							},
							o: {
								x: [0.167],
								y: [0.134]
							},
							t: 44,
							s: [1.042]
						},
						{
							i: {
								x: [0.833],
								y: [0.849]
							},
							o: {
								x: [0.167],
								y: [0.157]
							},
							t: 45,
							s: [0.959]
						},
						{
							i: {
								x: [0.833],
								y: [0.89]
							},
							o: {
								x: [0.167],
								y: [0.186]
							},
							t: 46,
							s: [0.865]
						},
						{
							i: {
								x: [0.833],
								y: [1.075]
							},
							o: {
								x: [0.167],
								y: [0.348]
							},
							t: 47,
							s: [0.789]
						},
						{
							i: {
								x: [0.833],
								y: [0.756]
							},
							o: {
								x: [0.167],
								y: [0.039]
							},
							t: 48,
							s: [0.765]
						},
						{
							i: {
								x: [0.833],
								y: [0.82]
							},
							o: {
								x: [0.167],
								y: [0.126]
							},
							t: 49,
							s: [0.811]
						},
						{
							i: {
								x: [0.833],
								y: [0.841]
							},
							o: {
								x: [0.167],
								y: [0.155]
							},
							t: 50,
							s: [0.898]
						},
						{
							i: {
								x: [0.833],
								y: [0.858]
							},
							o: {
								x: [0.167],
								y: [0.175]
							},
							t: 51,
							s: [1]
						},
						{
							i: {
								x: [0.833],
								y: [0.884]
							},
							o: {
								x: [0.167],
								y: [0.201]
							},
							t: 52,
							s: [1.093]
						},
						{
							i: {
								x: [0.833],
								y: [0.992]
							},
							o: {
								x: [0.167],
								y: [0.299]
							},
							t: 53,
							s: [1.159]
						},
						{
							i: {
								x: [0.833],
								y: [0.647]
							},
							o: {
								x: [0.167],
								y: [-0.008]
							},
							t: 54,
							s: [1.184]
						},
						{
							i: {
								x: [0.833],
								y: [0.778]
							},
							o: {
								x: [0.167],
								y: [0.109]
							},
							t: 55,
							s: [1.161]
						},
						{
							i: {
								x: [0.833],
								y: [0.805]
							},
							o: {
								x: [0.167],
								y: [0.133]
							},
							t: 56,
							s: [1.086]
						},
						{
							i: {
								x: [0.833],
								y: [0.818]
							},
							o: {
								x: [0.167],
								y: [0.145]
							},
							t: 57,
							s: [0.962]
						},
						{
							i: {
								x: [0.833],
								y: [0.827]
							},
							o: {
								x: [0.167],
								y: [0.154]
							},
							t: 58,
							s: [0.794]
						},
						{
							i: {
								x: [0.833],
								y: [0.836]
							},
							o: {
								x: [0.167],
								y: [0.161]
							},
							t: 59,
							s: [0.595]
						},
						{
							i: {
								x: [0.833],
								y: [0.844]
							},
							o: {
								x: [0.167],
								y: [0.169]
							},
							t: 60,
							s: [0.382]
						},
						{
							i: {
								x: [0.833],
								y: [0.854]
							},
							o: {
								x: [0.167],
								y: [0.179]
							},
							t: 61,
							s: [0.174]
						},
						{
							i: {
								x: [0.833],
								y: [0.869]
							},
							o: {
								x: [0.167],
								y: [0.195]
							},
							t: 62,
							s: [-0.005]
						},
						{
							i: {
								x: [0.833],
								y: [0.903]
							},
							o: {
								x: [0.167],
								y: [0.229]
							},
							t: 63,
							s: [-0.139]
						},
						{
							i: {
								x: [0.833],
								y: [1.261]
							},
							o: {
								x: [0.167],
								y: [0.59]
							},
							t: 64,
							s: [-0.216]
						},
						{
							i: {
								x: [0.833],
								y: [0.738]
							},
							o: {
								x: [0.167],
								y: [0.063]
							},
							t: 65,
							s: [-0.229]
						},
						{
							i: {
								x: [0.833],
								y: [0.797]
							},
							o: {
								x: [0.167],
								y: [0.122]
							},
							t: 66,
							s: [-0.177]
						},
						{
							i: {
								x: [0.833],
								y: [0.816]
							},
							o: {
								x: [0.167],
								y: [0.141]
							},
							t: 67,
							s: [-0.065]
						},
						{
							i: {
								x: [0.833],
								y: [0.829]
							},
							o: {
								x: [0.167],
								y: [0.153]
							},
							t: 68,
							s: [0.097]
						},
						{
							i: {
								x: [0.833],
								y: [0.84]
							},
							o: {
								x: [0.167],
								y: [0.162]
							},
							t: 69,
							s: [0.29]
						},
						{
							i: {
								x: [0.833],
								y: [0.856]
							},
							o: {
								x: [0.167],
								y: [0.174]
							},
							t: 70,
							s: [0.495]
						},
						{
							i: {
								x: [0.833],
								y: [0.887]
							},
							o: {
								x: [0.167],
								y: [0.198]
							},
							t: 71,
							s: [0.682]
						},
						{
							i: {
								x: [0.833],
								y: [0.929]
							},
							o: {
								x: [0.167],
								y: [0.316]
							},
							t: 72,
							s: [0.817]
						},
						{
							i: {
								x: [0.833],
								y: [0.469]
							},
							o: {
								x: [0.167],
								y: [-0.498]
							},
							t: 73,
							s: [0.866]
						},
						{
							i: {
								x: [0.833],
								y: [0.781]
							},
							o: {
								x: [0.167],
								y: [0.099]
							},
							t: 74,
							s: [0.859]
						},
						{
							i: {
								x: [0.833],
								y: [0.81]
							},
							o: {
								x: [0.167],
								y: [0.135]
							},
							t: 75,
							s: [0.822]
						},
						{
							i: {
								x: [0.833],
								y: [0.822]
							},
							o: {
								x: [0.167],
								y: [0.149]
							},
							t: 76,
							s: [0.761]
						},
						{
							i: {
								x: [0.833],
								y: [0.83]
							},
							o: {
								x: [0.167],
								y: [0.157]
							},
							t: 77,
							s: [0.683]
						},
						{
							i: {
								x: [0.833],
								y: [0.835]
							},
							o: {
								x: [0.167],
								y: [0.163]
							},
							t: 78,
							s: [0.595]
						},
						{
							i: {
								x: [0.833],
								y: [0.841]
							},
							o: {
								x: [0.167],
								y: [0.169]
							},
							t: 79,
							s: [0.503]
						},
						{
							i: {
								x: [0.833],
								y: [0.847]
							},
							o: {
								x: [0.167],
								y: [0.175]
							},
							t: 80,
							s: [0.413]
						},
						{
							i: {
								x: [0.833],
								y: [0.857]
							},
							o: {
								x: [0.167],
								y: [0.184]
							},
							t: 81,
							s: [0.331]
						},
						{
							i: {
								x: [0.833],
								y: [0.876]
							},
							o: {
								x: [0.167],
								y: [0.2]
							},
							t: 82,
							s: [0.264]
						},
						{
							i: {
								x: [0.833],
								y: [0.937]
							},
							o: {
								x: [0.167],
								y: [0.252]
							},
							t: 83,
							s: [0.215]
						},
						{
							i: {
								x: [0.833],
								y: [0.056]
							},
							o: {
								x: [0.167],
								y: [-0.261]
							},
							t: 84,
							s: [0.191]
						},
						{
							i: {
								x: [0.833],
								y: [0.727]
							},
							o: {
								x: [0.167],
								y: [0.091]
							},
							t: 85,
							s: [0.197]
						},
						{
							i: {
								x: [0.833],
								y: [0.809]
							},
							o: {
								x: [0.167],
								y: [0.12]
							},
							t: 86,
							s: [0.257]
						},
						{
							i: {
								x: [0.833],
								y: [0.83]
							},
							o: {
								x: [0.167],
								y: [0.148]
							},
							t: 87,
							s: [0.393]
						},
						{
							i: {
								x: [0.833],
								y: [0.843]
							},
							o: {
								x: [0.167],
								y: [0.164]
							},
							t: 88,
							s: [0.568]
						},
						{
							i: {
								x: [0.833],
								y: [0.855]
							},
							o: {
								x: [0.167],
								y: [0.177]
							},
							t: 89,
							s: [0.75]
						},
						{
							i: {
								x: [0.833],
								y: [0.872]
							},
							o: {
								x: [0.167],
								y: [0.195]
							},
							t: 90,
							s: [0.91]
						},
						{
							i: {
								x: [0.833],
								y: [0.916]
							},
							o: {
								x: [0.167],
								y: [0.239]
							},
							t: 91,
							s: [1.03]
						},
						{
							i: {
								x: [0.833],
								y: [9.606]
							},
							o: {
								x: [0.167],
								y: [8.68]
							},
							t: 92,
							s: [1.094]
						},
						{
							i: {
								x: [0.833],
								y: [0.755]
							},
							o: {
								x: [0.167],
								y: [0.083]
							},
							t: 93,
							s: [1.095]
						},
						{
							i: {
								x: [0.833],
								y: [0.8]
							},
							o: {
								x: [0.167],
								y: [0.126]
							},
							t: 94,
							s: [1.03]
						},
						{
							i: {
								x: [0.833],
								y: [0.817]
							},
							o: {
								x: [0.167],
								y: [0.143]
							},
							t: 95,
							s: [0.904]
						},
						{
							i: {
								x: [0.833],
								y: [0.828]
							},
							o: {
								x: [0.167],
								y: [0.153]
							},
							t: 96,
							s: [0.727]
						},
						{
							i: {
								x: [0.833],
								y: [0.826]
							},
							o: {
								x: [0.167],
								y: [0.162]
							},
							t: 97,
							s: [0.516]
						},
						{
							i: {
								x: [0.833],
								y: [0.831]
							},
							o: {
								x: [0.167],
								y: [0.16]
							},
							t: 98,
							s: [0.291]
						},
						{
							i: {
								x: [0.833],
								y: [0.838]
							},
							o: {
								x: [0.167],
								y: [0.164]
							},
							t: 99,
							s: [0.047]
						},
						{
							i: {
								x: [0.833],
								y: [0.844]
							},
							o: {
								x: [0.167],
								y: [0.171]
							},
							t: 100,
							s: [-0.206]
						},
						{
							i: {
								x: [0.833],
								y: [0.85]
							},
							o: {
								x: [0.167],
								y: [0.178]
							},
							t: 101,
							s: [-0.444]
						},
						{
							i: {
								x: [0.833],
								y: [0.856]
							},
							o: {
								x: [0.167],
								y: [0.187]
							},
							t: 102,
							s: [-0.653]
						},
						{
							i: {
								x: [0.833],
								y: [0.866]
							},
							o: {
								x: [0.167],
								y: [0.199]
							},
							t: 103,
							s: [-0.821]
						},
						{
							i: {
								x: [0.833],
								y: [0.883]
							},
							o: {
								x: [0.167],
								y: [0.221]
							},
							t: 104,
							s: [-0.943]
						},
						{
							i: {
								x: [0.833],
								y: [0.932]
							},
							o: {
								x: [0.167],
								y: [0.29]
							},
							t: 105,
							s: [-1.016]
						},
						{
							i: {
								x: [0.833],
								y: [0.509]
							},
							o: {
								x: [0.167],
								y: [-0.364]
							},
							t: 106,
							s: [-1.046]
						},
						{
							i: {
								x: [0.833],
								y: [0.824]
							},
							o: {
								x: [0.167],
								y: [0.1]
							},
							t: 107,
							s: [-1.041]
						},
						{
							i: {
								x: [0.833],
								y: [0.89]
							},
							o: {
								x: [0.167],
								y: [0.158]
							},
							t: 108,
							s: [-1.013]
						},
						{
							i: {
								x: [0.833],
								y: [1.146]
							},
							o: {
								x: [0.167],
								y: [0.348]
							},
							t: 109,
							s: [-0.983]
						},
						{
							i: {
								x: [0.833],
								y: [0.801]
							},
							o: {
								x: [0.167],
								y: [0.053]
							},
							t: 110,
							s: [-0.974]
						},
						{
							i: {
								x: [0.833],
								y: [0.852]
							},
							o: {
								x: [0.167],
								y: [0.144]
							},
							t: 111,
							s: [-1]
						},
						{
							i: {
								x: [0.833],
								y: [0.898]
							},
							o: {
								x: [0.167],
								y: [0.191]
							},
							t: 112,
							s: [-1.036]
						},
						{
							i: {
								x: [0.833],
								y: [1.256]
							},
							o: {
								x: [0.167],
								y: [0.459]
							},
							t: 113,
							s: [-1.064]
						},
						{
							i: {
								x: [0.833],
								y: [0.711]
							},
							o: {
								x: [0.167],
								y: [0.063]
							},
							t: 114,
							s: [-1.07]
						},
						{
							i: {
								x: [0.833],
								y: [0.781]
							},
							o: {
								x: [0.167],
								y: [0.117]
							},
							t: 115,
							s: [-1.045]
						},
						{
							i: {
								x: [0.833],
								y: [0.803]
							},
							o: {
								x: [0.167],
								y: [0.135]
							},
							t: 116,
							s: [-0.983]
						},
						{
							i: {
								x: [0.833],
								y: [0.815]
							},
							o: {
								x: [0.167],
								y: [0.145]
							},
							t: 117,
							s: [-0.881]
						},
						{
							i: {
								x: [0.833],
								y: [0.823]
							},
							o: {
								x: [0.167],
								y: [0.152]
							},
							t: 118,
							s: [-0.743]
						},
						{
							i: {
								x: [0.833],
								y: [0.831]
							},
							o: {
								x: [0.167],
								y: [0.158]
							},
							t: 119,
							s: [-0.574]
						},
						{
							i: {
								x: [0.833],
								y: [0.838]
							},
							o: {
								x: [0.167],
								y: [0.164]
							},
							t: 120,
							s: [-0.385]
						},
						{
							i: {
								x: [0.833],
								y: [0.849]
							},
							o: {
								x: [0.167],
								y: [0.172]
							},
							t: 121,
							s: [-0.19]
						},
						{
							i: {
								x: [0.833],
								y: [0.874]
							},
							o: {
								x: [0.167],
								y: [0.186]
							},
							t: 122,
							s: [-0.006]
						},
						{
							i: {
								x: [0.833],
								y: [0.911]
							},
							o: {
								x: [0.167],
								y: [0.245]
							},
							t: 123,
							s: [0.142]
						},
						{
							i: {
								x: [0.833],
								y: [1.76]
							},
							o: {
								x: [0.167],
								y: [1.3]
							},
							t: 124,
							s: [0.219]
						},
						{
							i: {
								x: [0.833],
								y: [0.763]
							},
							o: {
								x: [0.167],
								y: [0.075]
							},
							t: 125,
							s: [0.224]
						},
						{
							i: {
								x: [0.833],
								y: [0.807]
							},
							o: {
								x: [0.167],
								y: [0.128]
							},
							t: 126,
							s: [0.171]
						},
						{
							i: {
								x: [0.833],
								y: [0.822]
							},
							o: {
								x: [0.167],
								y: [0.146]
							},
							t: 127,
							s: [0.073]
						},
						{
							i: {
								x: [0.833],
								y: [0.832]
							},
							o: {
								x: [0.167],
								y: [0.157]
							},
							t: 128,
							s: [-0.056]
						},
						{
							i: {
								x: [0.833],
								y: [0.84]
							},
							o: {
								x: [0.167],
								y: [0.165]
							},
							t: 129,
							s: [-0.203]
						},
						{
							i: {
								x: [0.833],
								y: [0.849]
							},
							o: {
								x: [0.167],
								y: [0.174]
							},
							t: 130,
							s: [-0.352]
						},
						{
							i: {
								x: [0.833],
								y: [0.865]
							},
							o: {
								x: [0.167],
								y: [0.187]
							},
							t: 131,
							s: [-0.49]
						},
						{
							i: {
								x: [0.833],
								y: [0.903]
							},
							o: {
								x: [0.167],
								y: [0.218]
							},
							t: 132,
							s: [-0.601]
						},
						{
							i: {
								x: [0.833],
								y: [1.387]
							},
							o: {
								x: [0.167],
								y: [0.6]
							},
							t: 133,
							s: [-0.67]
						},
						{
							i: {
								x: [0.833],
								y: [0.681]
							},
							o: {
								x: [0.167],
								y: [0.069]
							},
							t: 134,
							s: [-0.681]
						},
						{
							i: {
								x: [0.833],
								y: [0.769]
							},
							o: {
								x: [0.167],
								y: [0.113]
							},
							t: 135,
							s: [-0.618]
						},
						{
							i: {
								x: [0.833],
								y: [0.814]
							},
							o: {
								x: [0.167],
								y: [0.13]
							},
							t: 136,
							s: [-0.441]
						},
						{
							i: {
								x: [0.833],
								y: [0.83]
							},
							o: {
								x: [0.167],
								y: [0.151]
							},
							t: 137,
							s: [-0.127]
						},
						{
							i: {
								x: [0.833],
								y: [0.84]
							},
							o: {
								x: [0.167],
								y: [0.164]
							},
							t: 138,
							s: [0.26]
						},
						{
							i: {
								x: [0.833],
								y: [0.849]
							},
							o: {
								x: [0.167],
								y: [0.174]
							},
							t: 139,
							s: [0.661]
						},
						{
							i: {
								x: [0.833],
								y: [0.859]
							},
							o: {
								x: [0.167],
								y: [0.185]
							},
							t: 140,
							s: [1.031]
						},
						{
							i: {
								x: [0.833],
								y: [0.877]
							},
							o: {
								x: [0.167],
								y: [0.204]
							},
							t: 141,
							s: [1.333]
						},
						{
							i: {
								x: [0.833],
								y: [0.927]
							},
							o: {
								x: [0.167],
								y: [0.256]
							},
							t: 142,
							s: [1.542]
						},
						{
							i: {
								x: [0.833],
								y: [0.104]
							},
							o: {
								x: [0.167],
								y: [-0.603]
							},
							t: 143,
							s: [1.642]
						},
						{
							i: {
								x: [0.833],
								y: [0.77]
							},
							o: {
								x: [0.167],
								y: [0.092]
							},
							t: 144,
							s: [1.63]
						},
						{
							i: {
								x: [0.833],
								y: [0.808]
							},
							o: {
								x: [0.167],
								y: [0.131]
							},
							t: 145,
							s: [1.511]
						},
						{
							i: {
								x: [0.833],
								y: [0.825]
							},
							o: {
								x: [0.167],
								y: [0.147]
							},
							t: 146,
							s: [1.301]
						},
						{
							i: {
								x: [0.833],
								y: [0.824]
							},
							o: {
								x: [0.167],
								y: [0.159]
							},
							t: 147,
							s: [1.028]
						},
						{
							i: {
								x: [0.833],
								y: [0.832]
							},
							o: {
								x: [0.167],
								y: [0.159]
							},
							t: 148,
							s: [0.726]
						},
						{
							i: {
								x: [0.833],
								y: [0.84]
							},
							o: {
								x: [0.167],
								y: [0.165]
							},
							t: 149,
							s: [0.391]
						},
						{
							i: {
								x: [0.833],
								y: [0.848]
							},
							o: {
								x: [0.167],
								y: [0.174]
							},
							t: 150,
							s: [0.05]
						},
						{
							i: {
								x: [0.833],
								y: [0.857]
							},
							o: {
								x: [0.167],
								y: [0.184]
							},
							t: 151,
							s: [-0.262]
						},
						{
							i: {
								x: [0.833],
								y: [0.87]
							},
							o: {
								x: [0.167],
								y: [0.2]
							},
							t: 152,
							s: [-0.52]
						},
						{
							i: {
								x: [0.833],
								y: [0.901]
							},
							o: {
								x: [0.167],
								y: [0.234]
							},
							t: 153,
							s: [-0.705]
						},
						{
							i: {
								x: [0.833],
								y: [1.159]
							},
							o: {
								x: [0.167],
								y: [0.526]
							},
							t: 154,
							s: [-0.807]
						},
						{
							i: {
								x: [0.833],
								y: [0.746]
							},
							o: {
								x: [0.167],
								y: [0.055]
							},
							t: 155,
							s: [-0.826]
						},
						{
							i: {
								x: [0.833],
								y: [0.809]
							},
							o: {
								x: [0.167],
								y: [0.124]
							},
							t: 156,
							s: [-0.77]
						},
						{
							i: {
								x: [0.833],
								y: [0.833]
							},
							o: {
								x: [0.167],
								y: [0.148]
							},
							t: 157,
							s: [-0.655]
						},
						{
							i: {
								x: [0.833],
								y: [0.857]
							},
							o: {
								x: [0.167],
								y: [0.167]
							},
							t: 158,
							s: [-0.506]
						},
						{
							i: {
								x: [0.833],
								y: [0.868]
							},
							o: {
								x: [0.167],
								y: [0.199]
							},
							t: 159,
							s: [-0.357]
						},
						{
							i: {
								x: [0.833],
								y: [0.794]
							},
							o: {
								x: [0.167],
								y: [0.225]
							},
							t: 160,
							s: [-0.25]
						},
						{
							i: {
								x: [0.833],
								y: [0.815]
							},
							o: {
								x: [0.167],
								y: [0.14]
							},
							t: 161,
							s: [-0.187]
						},
						{
							i: {
								x: [0.833],
								y: [0.826]
							},
							o: {
								x: [0.167],
								y: [0.152]
							},
							t: 162,
							s: [-0.094]
						},
						{
							i: {
								x: [0.833],
								y: [0.833]
							},
							o: {
								x: [0.167],
								y: [0.16]
							},
							t: 163,
							s: [0.019]
						},
						{
							i: {
								x: [0.833],
								y: [0.839]
							},
							o: {
								x: [0.167],
								y: [0.167]
							},
							t: 164,
							s: [0.142]
						},
						{
							i: {
								x: [0.833],
								y: [0.843]
							},
							o: {
								x: [0.167],
								y: [0.172]
							},
							t: 165,
							s: [0.265]
						},
						{
							i: {
								x: [0.833],
								y: [0.848]
							},
							o: {
								x: [0.167],
								y: [0.178]
							},
							t: 166,
							s: [0.38]
						},
						{
							i: {
								x: [0.833],
								y: [0.854]
							},
							o: {
								x: [0.167],
								y: [0.185]
							},
							t: 167,
							s: [0.481]
						},
						{
							i: {
								x: [0.833],
								y: [0.863]
							},
							o: {
								x: [0.167],
								y: [0.195]
							},
							t: 168,
							s: [0.565]
						},
						{
							i: {
								x: [0.833],
								y: [0.878]
							},
							o: {
								x: [0.167],
								y: [0.213]
							},
							t: 169,
							s: [0.627]
						},
						{
							i: {
								x: [0.833],
								y: [0.92]
							},
							o: {
								x: [0.167],
								y: [0.264]
							},
							t: 170,
							s: [0.667]
						},
						{
							i: {
								x: [0.833],
								y: [-0.469]
							},
							o: {
								x: [0.167],
								y: [-1.864]
							},
							t: 171,
							s: [0.685]
						},
						{
							i: {
								x: [0.833],
								y: [1.067]
							},
							o: {
								x: [0.167],
								y: [0.088]
							},
							t: 172,
							s: [0.685]
						},
						{
							i: {
								x: [0.833],
								y: [0.732]
							},
							o: {
								x: [0.167],
								y: [0.037]
							},
							t: 173,
							s: [0.672]
						},
						{
							i: {
								x: [0.833],
								y: [0.83]
							},
							o: {
								x: [0.167],
								y: [0.121]
							},
							t: 174,
							s: [0.695]
						},
						{
							i: {
								x: [0.833],
								y: [0.863]
							},
							o: {
								x: [0.167],
								y: [0.163]
							},
							t: 175,
							s: [0.748]
						},
						{
							i: {
								x: [0.833],
								y: [0.916]
							},
							o: {
								x: [0.167],
								y: [0.212]
							},
							t: 176,
							s: [0.803]
						},
						{
							i: {
								x: [0.833],
								y: [16.068]
							},
							o: {
								x: [0.167],
								y: [11.841]
							},
							t: 177,
							s: [0.839]
						},
						{
							i: {
								x: [0.833],
								y: [0.74]
							},
							o: {
								x: [0.167],
								y: [0.083]
							},
							t: 178,
							s: [0.839]
						},
						{
							i: {
								x: [0.833],
								y: [0.789]
							},
							o: {
								x: [0.167],
								y: [0.123]
							},
							t: 179,
							s: [0.793]
						},
						{
							i: {
								x: [0.833],
								y: [0.807]
							},
							o: {
								x: [0.167],
								y: [0.138]
							},
							t: 180,
							s: [0.695]
						},
						{
							i: {
								x: [0.833],
								y: [0.818]
							},
							o: {
								x: [0.167],
								y: [0.147]
							},
							t: 181,
							s: [0.546]
						},
						{
							i: {
								x: [0.833],
								y: [0.826]
							},
							o: {
								x: [0.167],
								y: [0.154]
							},
							t: 182,
							s: [0.35]
						},
						{
							i: {
								x: [0.833],
								y: [0.834]
							},
							o: {
								x: [0.167],
								y: [0.16]
							},
							t: 183,
							s: [0.118]
						},
						{
							i: {
								x: [0.833],
								y: [0.83]
							},
							o: {
								x: [0.167],
								y: [0.167]
							},
							t: 184,
							s: [-0.134]
						},
						{
							i: {
								x: [0.833],
								y: [0.818]
							},
							o: {
								x: [0.167],
								y: [0.163]
							},
							t: 185,
							s: [-0.385]
						},
						{
							i: {
								x: [0.833],
								y: [0.835]
							},
							o: {
								x: [0.167],
								y: [0.154]
							},
							t: 186,
							s: [-0.648]
						},
						{
							i: {
								x: [0.833],
								y: [0.848]
							},
							o: {
								x: [0.167],
								y: [0.168]
							},
							t: 187,
							s: [-0.958]
						},
						{
							i: {
								x: [0.833],
								y: [0.862]
							},
							o: {
								x: [0.167],
								y: [0.184]
							},
							t: 188,
							s: [-1.263]
						},
						{
							i: {
								x: [0.833],
								y: [0.888]
							},
							o: {
								x: [0.167],
								y: [0.21]
							},
							t: 189,
							s: [-1.514]
						},
						{
							i: {
								x: [0.833],
								y: [1.002]
							},
							o: {
								x: [0.167],
								y: [0.322]
							},
							t: 190,
							s: [-1.68]
						},
						{
							i: {
								x: [0.833],
								y: [0.673]
							},
							o: {
								x: [0.167],
								y: [0.002]
							},
							t: 191,
							s: [-1.738]
						},
						{
							i: {
								x: [0.833],
								y: [0.786]
							},
							o: {
								x: [0.167],
								y: [0.112]
							},
							t: 192,
							s: [-1.678]
						},
						{
							i: {
								x: [0.833],
								y: [0.811]
							},
							o: {
								x: [0.167],
								y: [0.136]
							},
							t: 193,
							s: [-1.504]
						},
						{
							i: {
								x: [0.833],
								y: [0.825]
							},
							o: {
								x: [0.167],
								y: [0.149]
							},
							t: 194,
							s: [-1.23]
						},
						{
							i: {
								x: [0.833],
								y: [0.837]
							},
							o: {
								x: [0.167],
								y: [0.159]
							},
							t: 195,
							s: [-0.883]
						},
						{
							i: {
								x: [0.833],
								y: [0.851]
							},
							o: {
								x: [0.167],
								y: [0.171]
							},
							t: 196,
							s: [-0.503]
						},
						{
							i: {
								x: [0.833],
								y: [0.861]
							},
							o: {
								x: [0.167],
								y: [0.19]
							},
							t: 197,
							s: [-0.14]
						},
						{
							i: {
								x: [0.833],
								y: [0.868]
							},
							o: {
								x: [0.167],
								y: [0.208]
							},
							t: 198,
							s: [0.144]
						},
						{
							i: {
								x: [0.833],
								y: [0.882]
							},
							o: {
								x: [0.167],
								y: [0.225]
							},
							t: 199,
							s: [0.334]
						},
						{
							i: {
								x: [0.833],
								y: [0.926]
							},
							o: {
								x: [0.167],
								y: [0.284]
							},
							t: 200,
							s: [0.445]
						},
						{
							i: {
								x: [0.833],
								y: [0.247]
							},
							o: {
								x: [0.167],
								y: [-0.643]
							},
							t: 201,
							s: [0.492]
						},
						{
							i: {
								x: [0.833],
								y: [0.79]
							},
							o: {
								x: [0.167],
								y: [0.094]
							},
							t: 202,
							s: [0.486]
						},
						{
							i: {
								x: [0.833],
								y: [0.825]
							},
							o: {
								x: [0.167],
								y: [0.138]
							},
							t: 203,
							s: [0.444]
						},
						{
							i: {
								x: [0.833],
								y: [0.845]
							},
							o: {
								x: [0.167],
								y: [0.159]
							},
							t: 204,
							s: [0.378]
						},
						{
							i: {
								x: [0.833],
								y: [0.869]
							},
							o: {
								x: [0.167],
								y: [0.18]
							},
							t: 205,
							s: [0.306]
						},
						{
							i: {
								x: [0.833],
								y: [0.94]
							},
							o: {
								x: [0.167],
								y: [0.231]
							},
							t: 206,
							s: [0.244]
						},
						{
							i: {
								x: [0.833],
								y: [0.29]
							},
							o: {
								x: [0.167],
								y: [-0.217]
							},
							t: 207,
							s: [0.209]
						},
						{
							i: {
								x: [0.833],
								y: [0.739]
							},
							o: {
								x: [0.167],
								y: [0.094]
							},
							t: 208,
							s: [0.219]
						},
						{
							i: {
								x: [0.833],
								y: [0.823]
							},
							o: {
								x: [0.167],
								y: [0.122]
							},
							t: 209,
							s: [0.292]
						},
						{
							i: {
								x: [0.833],
								y: [0.899]
							},
							o: {
								x: [0.167],
								y: [0.158]
							},
							t: 210,
							s: [0.449]
						},
						{
							i: {
								x: [0.833],
								y: [1.09]
							},
							o: {
								x: [0.167],
								y: [0.483]
							},
							t: 211,
							s: [0.624]
						},
						{
							i: {
								x: [0.833],
								y: [0.744]
							},
							o: {
								x: [0.167],
								y: [0.043]
							},
							t: 212,
							s: [0.661]
						},
						{
							i: {
								x: [0.833],
								y: [0.806]
							},
							o: {
								x: [0.167],
								y: [0.124]
							},
							t: 213,
							s: [0.585]
						},
						{
							i: {
								x: [0.833],
								y: [0.824]
							},
							o: {
								x: [0.167],
								y: [0.146]
							},
							t: 214,
							s: [0.428]
						},
						{
							i: {
								x: [0.833],
								y: [0.835]
							},
							o: {
								x: [0.167],
								y: [0.158]
							},
							t: 215,
							s: [0.219]
						},
						{
							i: {
								x: [0.833],
								y: [0.844]
							},
							o: {
								x: [0.167],
								y: [0.168]
							},
							t: 216,
							s: [-0.013]
						},
						{
							i: {
								x: [0.833],
								y: [0.855]
							},
							o: {
								x: [0.167],
								y: [0.179]
							},
							t: 217,
							s: [-0.241]
						},
						{
							i: {
								x: [0.833],
								y: [0.875]
							},
							o: {
								x: [0.167],
								y: [0.196]
							},
							t: 218,
							s: [-0.44]
						},
						{
							i: {
								x: [0.833],
								y: [0.94]
							},
							o: {
								x: [0.167],
								y: [0.251]
							},
							t: 219,
							s: [-0.588]
						},
						{
							i: {
								x: [0.833],
								y: [0.379]
							},
							o: {
								x: [0.167],
								y: [-0.213]
							},
							t: 220,
							s: [-0.661]
						},
						{
							i: {
								x: [0.833],
								y: [0.754]
							},
							o: {
								x: [0.167],
								y: [0.096]
							},
							t: 221,
							s: [-0.64]
						},
						{
							i: {
								x: [0.833],
								y: [0.806]
							},
							o: {
								x: [0.167],
								y: [0.126]
							},
							t: 222,
							s: [-0.507]
						},
						{
							i: {
								x: [0.833],
								y: [0.828]
							},
							o: {
								x: [0.167],
								y: [0.146]
							},
							t: 223,
							s: [-0.246]
						},
						{
							i: {
								x: [0.833],
								y: [0.839]
							},
							o: {
								x: [0.167],
								y: [0.162]
							},
							t: 224,
							s: [0.101]
						},
						{
							i: {
								x: [0.833],
								y: [0.847]
							},
							o: {
								x: [0.167],
								y: [0.172]
							},
							t: 225,
							s: [0.47]
						},
						{
							i: {
								x: [0.833],
								y: [0.857]
							},
							o: {
								x: [0.167],
								y: [0.184]
							},
							t: 226,
							s: [0.816]
						},
						{
							i: {
								x: [0.833],
								y: [0.873]
							},
							o: {
								x: [0.167],
								y: [0.201]
							},
							t: 227,
							s: [1.104]
						},
						{
							i: {
								x: [0.833],
								y: [0.913]
							},
							o: {
								x: [0.167],
								y: [0.243]
							},
							t: 228,
							s: [1.308]
						},
						{
							i: {
								x: [0.833],
								y: [2.44]
							},
							o: {
								x: [0.167],
								y: [1.886]
							},
							t: 229,
							s: [1.415]
						},
						{
							i: {
								x: [0.833],
								y: [0.761]
							},
							o: {
								x: [0.167],
								y: [0.079]
							},
							t: 230,
							s: [1.42]
						},
						{
							i: {
								x: [0.833],
								y: [0.808]
							},
							o: {
								x: [0.167],
								y: [0.128]
							},
							t: 231,
							s: [1.33]
						},
						{
							i: {
								x: [0.833],
								y: [0.829]
							},
							o: {
								x: [0.167],
								y: [0.147]
							},
							t: 232,
							s: [1.162]
						},
						{
							i: {
								x: [0.833],
								y: [0.847]
							},
							o: {
								x: [0.167],
								y: [0.162]
							},
							t: 233,
							s: [0.944]
						},
						{
							i: {
								x: [0.833],
								y: [0.868]
							},
							o: {
								x: [0.167],
								y: [0.182]
							},
							t: 234,
							s: [0.714]
						},
						{
							i: {
								x: [0.833],
								y: [0.883]
							},
							o: {
								x: [0.167],
								y: [0.226]
							},
							t: 235,
							s: [0.521]
						},
						{
							i: {
								x: [0.833],
								y: [0.923]
							},
							o: {
								x: [0.167],
								y: [0.29]
							},
							t: 236,
							s: [0.408]
						},
						{
							i: {
								x: [0.833],
								y: [0.025]
							},
							o: {
								x: [0.167],
								y: [-1.056]
							},
							t: 237,
							s: [0.362]
						},
						{
							i: {
								x: [0.833],
								y: [0.793]
							},
							o: {
								x: [0.167],
								y: [0.091]
							},
							t: 238,
							s: [0.365]
						},
						{
							i: {
								x: [0.833],
								y: [0.828]
							},
							o: {
								x: [0.167],
								y: [0.14]
							},
							t: 239,
							s: [0.401]
						},
						{
							i: {
								x: [0.833],
								y: [0.846]
							},
							o: {
								x: [0.167],
								y: [0.161]
							},
							t: 240,
							s: [0.454]
						},
						{
							i: {
								x: [0.833],
								y: [0.867]
							},
							o: {
								x: [0.167],
								y: [0.182]
							},
							t: 241,
							s: [0.511]
						},
						{
							i: {
								x: [0.833],
								y: [0.918]
							},
							o: {
								x: [0.167],
								y: [0.224]
							},
							t: 242,
							s: [0.558]
						},
						{
							i: {
								x: [0.833],
								y: [-6.669]
							},
							o: {
								x: [0.167],
								y: [-5.718]
							},
							t: 243,
							s: [0.587]
						},
						{
							i: {
								x: [0.833],
								y: [0.736]
							},
							o: {
								x: [0.167],
								y: [0.084]
							},
							t: 244,
							s: [0.586]
						},
						{
							i: {
								x: [0.833],
								y: [0.783]
							},
							o: {
								x: [0.167],
								y: [0.122]
							},
							t: 245,
							s: [0.549]
						},
						{
							i: {
								x: [0.833],
								y: [0.803]
							},
							o: {
								x: [0.167],
								y: [0.135]
							},
							t: 246,
							s: [0.469]
						},
						{
							i: {
								x: [0.833],
								y: [0.849]
							},
							o: {
								x: [0.167],
								y: [0.144]
							},
							t: 247,
							s: [0.34]
						},
						{
							i: {
								x: [0.833],
								y: [0.87]
							},
							o: {
								x: [0.167],
								y: [0.187]
							},
							t: 248,
							s: [0.165]
						},
						{
							i: {
								x: [0.833],
								y: [0.895]
							},
							o: {
								x: [0.167],
								y: [0.232]
							},
							t: 249,
							s: [0.023]
						},
						{
							i: {
								x: [0.833],
								y: [1.045]
							},
							o: {
								x: [0.167],
								y: [0.403]
							},
							t: 250,
							s: [-0.057]
						},
						{
							i: {
								x: [0.833],
								y: [0.718]
							},
							o: {
								x: [0.167],
								y: [0.029]
							},
							t: 251,
							s: [-0.077]
						},
						{
							i: {
								x: [0.833],
								y: [0.797]
							},
							o: {
								x: [0.167],
								y: [0.118]
							},
							t: 252,
							s: [-0.046]
						},
						{
							i: {
								x: [0.833],
								y: [0.817]
							},
							o: {
								x: [0.167],
								y: [0.141]
							},
							t: 253,
							s: [0.03]
						},
						{
							i: {
								x: [0.833],
								y: [0.83]
							},
							o: {
								x: [0.167],
								y: [0.153]
							},
							t: 254,
							s: [0.14]
						},
						{
							i: {
								x: [0.833],
								y: [0.84]
							},
							o: {
								x: [0.167],
								y: [0.163]
							},
							t: 255,
							s: [0.27]
						},
						{
							i: {
								x: [0.833],
								y: [0.854]
							},
							o: {
								x: [0.167],
								y: [0.174]
							},
							t: 256,
							s: [0.406]
						},
						{
							i: {
								x: [0.833],
								y: [0.879]
							},
							o: {
								x: [0.167],
								y: [0.194]
							},
							t: 257,
							s: [0.531]
						},
						{
							i: {
								x: [0.833],
								y: [0.983]
							},
							o: {
								x: [0.167],
								y: [0.27]
							},
							t: 258,
							s: [0.625]
						},
						{
							i: {
								x: [0.833],
								y: [0.728]
							},
							o: {
								x: [0.167],
								y: [-0.021]
							},
							t: 259,
							s: [0.667]
						},
						{
							i: {
								x: [0.833],
								y: [0.903]
							},
							o: {
								x: [0.167],
								y: [0.12]
							},
							t: 260,
							s: [0.634]
						},
						{
							i: {
								x: [0.833],
								y: [1.145]
							},
							o: {
								x: [0.167],
								y: [0.59]
							},
							t: 261,
							s: [0.558]
						},
						{
							i: {
								x: [0.833],
								y: [0.766]
							},
							o: {
								x: [0.167],
								y: [0.053]
							},
							t: 262,
							s: [0.545]
						},
						{
							i: {
								x: [0.833],
								y: [0.819]
							},
							o: {
								x: [0.167],
								y: [0.129]
							},
							t: 263,
							s: [0.579]
						},
						{
							i: {
								x: [0.833],
								y: [0.838]
							},
							o: {
								x: [0.167],
								y: [0.154]
							},
							t: 264,
							s: [0.642]
						},
						{
							i: {
								x: [0.833],
								y: [0.853]
							},
							o: {
								x: [0.167],
								y: [0.171]
							},
							t: 265,
							s: [0.715]
						},
						{
							i: {
								x: [0.833],
								y: [0.875]
							},
							o: {
								x: [0.167],
								y: [0.192]
							},
							t: 266,
							s: [0.784]
						},
						{
							i: {
								x: [0.833],
								y: [0.944]
							},
							o: {
								x: [0.167],
								y: [0.251]
							},
							t: 267,
							s: [0.837]
						},
						{
							i: {
								x: [0.833],
								y: [0.436]
							},
							o: {
								x: [0.167],
								y: [-0.173]
							},
							t: 268,
							s: [0.863]
						},
						{
							i: {
								x: [0.833],
								y: [0.758]
							},
							o: {
								x: [0.167],
								y: [0.098]
							},
							t: 269,
							s: [0.854]
						},
						{
							i: {
								x: [0.833],
								y: [0.793]
							},
							o: {
								x: [0.167],
								y: [0.127]
							},
							t: 270,
							s: [0.805]
						},
						{
							i: {
								x: [0.833],
								y: [0.806]
							},
							o: {
								x: [0.167],
								y: [0.139]
							},
							t: 271,
							s: [0.712]
						},
						{
							i: {
								x: [0.833],
								y: [0.816]
							},
							o: {
								x: [0.167],
								y: [0.146]
							},
							t: 272,
							s: [0.573]
						},
						{
							i: {
								x: [0.833],
								y: [0.833]
							},
							o: {
								x: [0.167],
								y: [0.152]
							},
							t: 273,
							s: [0.39]
						},
						{
							t: 274,
							s: [0.167]
						}
					],
					ix: 10,
					x: "var $bm_rt;\nvar deltaIndex;\ndeltaIndex = Math.max(Math.min($bm_sub(effect('Today')('Slider'), effect('Forecast')('Slider')), 50), -50);\n$bm_rt = $bm_sum($bm_div($bm_mul(Math.asin($bm_div($bm_div($bm_mul(600, deltaIndex), 50), 1754)), 180), Math.PI), value);"
				},
				p: {
					a: 1,
					k: [
						{
							i: {
								x: 0.01,
								y: 1
							},
							o: {
								x: 0.66,
								y: 0
							},
							t: 75,
							s: [1716.293, 651, 0],
							to: [0, -30, 0],
							ti: [0, 30, 0]
						},
						{
							i: {
								x: 0.01,
								y: 0.01
							},
							o: {
								x: 0.167,
								y: 0.167
							},
							t: 99,
							s: [1716.293, 471, 0],
							to: [0, 0, 0],
							ti: [0, 0, 0]
						},
						{
							i: {
								x: 0.01,
								y: 1
							},
							o: {
								x: 0.66,
								y: 0
							},
							t: 150,
							s: [1716.293, 471, 0],
							to: [0, -16.667, 0],
							ti: [0, 16.667, 0]
						},
						{
							t: 174,
							s: [1716.293, 371, 0]
						}
					],
					ix: 2,
					x: "var $bm_rt;\n$bm_rt = $bm_sum(value, [\n    0,\n    150 * Math.max(Math.min(effect('Today')('Slider') - effect('Forecast')('Slider'), 50), -50) / 50\n]);",
					l: 2
				},
				a: {
					a: 0,
					k: [0, 0, 0],
					ix: 1,
					l: 2
				},
				s: {
					a: 0,
					k: [100, 100, 100],
					ix: 6,
					l: 2
				}
			},
			ao: 0,
			ef: [
				{
					ty: 5,
					nm: "Today",
					np: 3,
					mn: "ADBE Slider Control",
					ix: 1,
					en: 1,
					ef: [
						{
							ty: 0,
							nm: "Slider",
							mn: "ADBE Slider Control-0001",
							ix: 1,
							v: {
								a: 0,
								k: 20,
								ix: 1
							}
						}
					]
				},
				{
					ty: 5,
					nm: "Forecast",
					np: 3,
					mn: "ADBE Slider Control",
					ix: 2,
					en: 1,
					ef: [
						{
							ty: 0,
							nm: "Slider",
							mn: "ADBE Slider Control-0001",
							ix: 1,
							v: {
								a: 0,
								k: 8,
								ix: 1
							}
						}
					]
				},
				{
					ty: 5,
					nm: "WiggleAmp",
					np: 3,
					mn: "ADBE Slider Control",
					ix: 3,
					en: 1,
					ef: [
						{
							ty: 0,
							nm: "Slider",
							mn: "ADBE Slider Control-0001",
							ix: 1,
							v: {
								a: 1,
								k: [
									{
										i: {
											x: [0.01],
											y: [1]
										},
										o: {
											x: [0.66],
											y: [0]
										},
										t: 274,
										s: [2]
									},
									{
										t: 298,
										s: [0]
									}
								],
								ix: 1
							}
						}
					]
				}
			],
			shapes: [
				{
					ty: "gr",
					it: [
						{
							ind: 0,
							ty: "sh",
							ix: 1,
							ks: {
								a: 0,
								k: {
									i: [
										[0, 0],
										[0, 0]
									],
									o: [
										[0, 0],
										[0, 0]
									],
									v: [
										[-1352.292, 3],
										[-0.292, 3]
									],
									c: false
								},
								ix: 2
							},
							nm: "Path 1",
							mn: "ADBE Vector Shape - Group",
							hd: false
						},
						{
							ty: "tm",
							s: {
								a: 0,
								k: 99.9,
								ix: 1
							},
							e: {
								a: 0,
								k: 100,
								ix: 2
							},
							o: {
								a: 0,
								k: 354,
								ix: 3,
								x: "var $bm_rt;\n$bm_rt = Math.max($bm_mul($bm_div(content('Fascio').content('Trim Paths 1').end, 100), 360), 1);"
							},
							m: 1,
							ix: 2,
							nm: "Trim Paths 1",
							mn: "ADBE Vector Filter - Trim",
							hd: false
						},
						{
							ty: "st",
							c: {
								a: 0,
								k: [0.945098042488, 0.945098042488, 0.945098042488, 1],
								ix: 3
							},
							o: {
								a: 0,
								k: 100,
								ix: 4
							},
							w: {
								a: 0,
								k: 210,
								ix: 5
							},
							lc: 2,
							lj: 1,
							ml: 4,
							bm: 0,
							nm: "Stroke 1",
							mn: "ADBE Vector Graphic - Stroke",
							hd: false
						},
						{
							ty: "tr",
							p: {
								a: 0,
								k: [0, 0],
								ix: 2
							},
							a: {
								a: 0,
								k: [0, 0],
								ix: 1
							},
							s: {
								a: 0,
								k: [100, 100],
								ix: 3
							},
							r: {
								a: 0,
								k: 0,
								ix: 6
							},
							o: {
								a: 0,
								k: 100,
								ix: 7
							},
							sk: {
								a: 0,
								k: 0,
								ix: 4
							},
							sa: {
								a: 0,
								k: 0,
								ix: 5
							},
							nm: "Transform"
						}
					],
					nm: "Cerchio",
					np: 3,
					cix: 2,
					bm: 0,
					ix: 1,
					mn: "ADBE Vector Group",
					hd: false
				},
				{
					ty: "gr",
					it: [
						{
							ind: 0,
							ty: "sh",
							ix: 1,
							ks: {
								a: 0,
								k: {
									i: [
										[0, 0],
										[0, 0]
									],
									o: [
										[0, 0],
										[0, 0]
									],
									v: [
										[-1352.292, 3],
										[-0.292, 3]
									],
									c: false
								},
								ix: 2
							},
							nm: "Path 1",
							mn: "ADBE Vector Shape - Group",
							hd: false
						},
						{
							ty: "tm",
							s: {
								a: 0,
								k: 0,
								ix: 1
							},
							e: {
								a: 1,
								k: [
									{
										i: {
											x: [0.01],
											y: [1]
										},
										o: {
											x: [0.66],
											y: [0]
										},
										t: 25,
										s: [0]
									},
									{
										t: 55,
										s: [100]
									}
								],
								ix: 2
							},
							o: {
								a: 0,
								k: 0,
								ix: 3
							},
							m: 1,
							ix: 2,
							nm: "Trim Paths 1",
							mn: "ADBE Vector Filter - Trim",
							hd: false
						},
						{
							ty: "st",
							c: {
								a: 0,
								k: [0.160784313725, 0.8, 0.694117647059, 1],
								ix: 3,
								x: "var $bm_rt;\n$bm_rt = thisComp.layer('VALUE_1').effect('Color')('Color');"
							},
							o: {
								a: 0,
								k: 100,
								ix: 4
							},
							w: {
								a: 0,
								k: 210,
								ix: 5
							},
							lc: 1,
							lj: 1,
							ml: 4,
							bm: 0,
							nm: "Stroke 1",
							mn: "ADBE Vector Graphic - Stroke",
							hd: false
						},
						{
							ty: "tr",
							p: {
								a: 0,
								k: [0, 0],
								ix: 2
							},
							a: {
								a: 0,
								k: [0, 0],
								ix: 1
							},
							s: {
								a: 0,
								k: [100, 100],
								ix: 3
							},
							r: {
								a: 0,
								k: 0,
								ix: 6
							},
							o: {
								a: 0,
								k: 100,
								ix: 7
							},
							sk: {
								a: 0,
								k: 0,
								ix: 4
							},
							sa: {
								a: 0,
								k: 0,
								ix: 5
							},
							nm: "Transform"
						}
					],
					nm: "Fascio",
					np: 3,
					cix: 2,
					bm: 0,
					ix: 2,
					mn: "ADBE Vector Group",
					hd: false
				}
			],
			ip: 25,
			op: 275,
			st: 25,
			bm: 0
		},
		{
			ddd: 0,
			ind: 26,
			ty: 4,
			nm: "BG",
			sr: 1,
			ks: {
				o: {
					a: 0,
					k: 100,
					ix: 11
				},
				r: {
					a: 0,
					k: 0,
					ix: 10
				},
				p: {
					a: 0,
					k: [945, 675, 0],
					ix: 2,
					l: 2
				},
				a: {
					a: 0,
					k: [0, 0, 0],
					ix: 1,
					l: 2
				},
				s: {
					a: 0,
					k: [100, 100, 100],
					ix: 6,
					l: 2
				}
			},
			ao: 0,
			shapes: [
				{
					ty: "gr",
					it: [
						{
							ty: "rc",
							d: 1,
							s: {
								a: 0,
								k: [1890, 1350],
								ix: 2
							},
							p: {
								a: 0,
								k: [0, 0],
								ix: 3
							},
							r: {
								a: 0,
								k: 0,
								ix: 4
							},
							nm: "Rectangle Path 1",
							mn: "ADBE Vector Shape - Rect",
							hd: false
						},
						{
							ty: "fl",
							c: {
								a: 0,
								k: [0, 0, 0, 1],
								ix: 4
							},
							o: {
								a: 0,
								k: 100,
								ix: 5
							},
							r: 1,
							bm: 0,
							nm: "Fill 1",
							mn: "ADBE Vector Graphic - Fill",
							hd: false
						},
						{
							ty: "tr",
							p: {
								a: 0,
								k: [0, 0],
								ix: 2
							},
							a: {
								a: 0,
								k: [0, 0],
								ix: 1
							},
							s: {
								a: 0,
								k: [100, 100],
								ix: 3
							},
							r: {
								a: 0,
								k: 0,
								ix: 6
							},
							o: {
								a: 0,
								k: 100,
								ix: 7
							},
							sk: {
								a: 0,
								k: 0,
								ix: 4
							},
							sa: {
								a: 0,
								k: 0,
								ix: 5
							},
							nm: "Transform"
						}
					],
					nm: "Rectangle 1",
					np: 3,
					cix: 2,
					bm: 0,
					ix: 1,
					mn: "ADBE Vector Group",
					hd: false
				}
			],
			ip: 0,
			op: 250,
			st: 0,
			bm: 0
		}
	],
	markers: []
};

export default animation;
