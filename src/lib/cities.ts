const cities = [
	"Ahmedabad",
	"Bangalore",
	"Bangkok",
	"Beijing",
	"Bogota",
	"Buenos Aires",
	"Chengdu",
	"Chennai",
	"Chongqing",
	"Foshan",
	"Guangzhou",
	"Hangzhou",
	"Hong Kong",
	"London",
	"Madrid",
	"Mexico City",
	"Nagoya",
	"Nanjing",
	"Osaka",
	"Paris",
	"Pune",
	"Roma",
	"Santiago",
	"Sao Paulo",
	"Seoul",
	"Shanghai",
	"Shenyang",
	"Shenzhen",
	"Tianjin",
	"Tokyo"
];

export default cities;
