import animation from "$lib/animation";

const aqiColors = {
	Hazardous: "#952cc7",
	VeryUnhealthy: "#f85a28",
	Unhealthy: "#f3b53f",
	Moderate: "#bad97a",
	Good: "#57bdab"
};

/** @type {import('./[city=city]').RequestHandler} */
export async function get({ params: { city } }: { params: { city: string } }) {
	const res = await fetch(
		`http://api.waqi.info/feed/${city}/?token=${import.meta.env.VITE_AQICN_TOKEN}`
	).then((res) => res.json());

	let animationData = animation;

	if (res.status !== "ok") return { status: 500 };

	animationData.layers[0].t.d.k[0].s.t = city;
	animationData.layers[3].t.d.k[0].s.t = new Date(res.data.time.v * 1000).toLocaleDateString(
		"it"
	);
	animationData.layers[8].ef[0].ef[0].v.k = res.data.aqi || 0;
	animationData.layers[24].ef[0].ef[0].v.k = res.data.iaqi.o3.v || 0;
	animationData.layers[24].ef[1].ef[0].v.k = res.data.forecast.daily.o3[0].avg || 0;
	animationData.layers[23].ef[0].ef[0].v.k = res.data.iaqi.pm10.v || 0;
	animationData.layers[23].ef[1].ef[0].v.k = res.data.forecast.daily.pm10[0].avg || 0;
	animationData.layers[21].ef[0].ef[0].v.k = res.data.iaqi.pm25.v || 0;
	animationData.layers[21].ef[1].ef[0].v.k = res.data.forecast.daily.pm25[0].avg || 0;

	const aqiColor =
		res.data.aqi > 300
			? aqiColors.Hazardous
			: res.data.aqi > 200
			? aqiColors.VeryUnhealthy
			: res.data.aqi > 100
			? aqiColors.Unhealthy
			: res.data.aqi > 50
			? aqiColors.Moderate
			: aqiColors.Good;

	return {
		body: { animationData, aqiColor }
	};
}
