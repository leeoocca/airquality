import cities from "$lib/cities";

/** @type {import('@sveltejs/kit').ParamMatcher} */
export function match(param: string) {
	return cities.includes(param.replace("%20", " "));
}
